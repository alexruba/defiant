﻿using UnityEngine;
using System.Collections;

public class MusicSingleton : MonoBehaviour
{
	public static MusicSingleton instance;
	
	public AudioClip musicMainMenu;
	public AudioSource audioSource;
	private int lastLevel;

	void Start()
	{
		audioSource.clip = musicMainMenu;
		audioSource.Play();
	}

	void Awake() 
	{
		if ( instance != null && instance != this ) 
		{
			Destroy( this.gameObject );
			return;
		} 
		else 
		{
			instance = this;
		}
		
		DontDestroyOnLoad( this.gameObject );
	}
	
	void OnLevelWasLoaded(int level)
	{
		if (lastLevel == 2)
		{
			audioSource.clip = musicMainMenu;
			audioSource.Play();
		}

		lastLevel = level;
	}
	
	public static MusicSingleton GetInstance()  
	{
		return instance;
	}
	
	void Update() 
	{
		//
	}
	
}