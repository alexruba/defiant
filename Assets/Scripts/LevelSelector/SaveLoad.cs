﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad {
	
	public static void SaveAllInformation()
	{
		if (GameInformation.CurrentGame != null) {
			GameInformation.CurrentGame.ModsLockStatus = new List<List<bool>> ();
			List<bool> modsAux = new List<bool> ();
			GameInformation.CurrentGame.DefiantsLockStatus = new List<bool> ();
			GameInformation.CurrentGame.DefiantsAvailableStatus = new List<bool> ();
			for (int i = 0; i < GameInformation.CurrentGame.Defiants.Count; i++) {
				modsAux = new List<bool>();
				GameInformation.CurrentGame.DefiantsLockStatus.Add (GameInformation.CurrentGame.Defiants[i].Locked);
				GameInformation.CurrentGame.DefiantsAvailableStatus.Add(GameInformation.CurrentGame.Defiants[i].Available);

				for(int j = 0; j < GameInformation.CurrentGame.Defiants[i].Mods.Count; j++)
					modsAux.Add(GameInformation.CurrentGame.Defiants[i].Mods[j].Locked);

				GameInformation.CurrentGame.ModsLockStatus.Add(modsAux);
			}

			PPSerialization.Save ("GAME", GameInformation.CurrentGame);
		}
		else
			PPSerialization.Save ("GAME", new Game ());
	}

	public static void LoadAllInformation()
	{
		if (PlayerPrefs.GetString ("GAME") != string.Empty) {
			GameInformation.CurrentGame = (Game)PPSerialization.Load ("GAME");
			
			List<DefiantMod> mods = new List<DefiantMod> ();
			List<Defiant> defiants = new List<Defiant> ();
			mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/default"), Resources.Load<Sprite> ("sprites/Skins/Classic/default"), 0, false, DefiantMod.Type.DEFAULT));
			mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/powered"), Resources.Load<Sprite> ("sprites/Skins/Classic/powered"), 1000, true, DefiantMod.Type.POWERED));
			mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/master"), Resources.Load<Sprite> ("sprites/Skins/Classic/master"), 2000, true, DefiantMod.Type.MASTER));
			mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/gold"), Resources.Load<Sprite> ("sprites/Skins/Classic/gold"), 4000, true, DefiantMod.Type.GOLD));
			defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Classic/default"), false, true, 0.0f, Defiant.Type.CLASSIC, Resources.Load<Sprite>("sprites/Skins/Classic/default"), Defiant.SuperType.BULLET, mods, "Classic"));
			
			List<DefiantMod> mods2 = new List<DefiantMod> ();
			mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/default"), Resources.Load<Sprite> ("sprites/Skins/Alpha/default"), 0, false, DefiantMod.Type.DEFAULT));
			mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/powered"), Resources.Load<Sprite> ("sprites/Skins/Alpha/powered"), 1000, true, DefiantMod.Type.POWERED));
			mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/master"), Resources.Load<Sprite> ("sprites/Skins/Alpha/master"), 2000, true, DefiantMod.Type.MASTER));
			mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/gold"), Resources.Load<Sprite> ("sprites/Skins/Alpha/gold"), 4000, true, DefiantMod.Type.GOLD));
			defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/default"), true, false, 150, Defiant.Type.ALPHA, Resources.Load<Sprite>("sprites/Skins/Alpha/default"), Defiant.SuperType.BULLET, mods2, "Alpha"));
			
			List<DefiantMod> mods3 = new List<DefiantMod> ();
			mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/default"), Resources.Load<Sprite> ("sprites/Skins/Omium/default"), 0, false, DefiantMod.Type.DEFAULT));
			mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/powered"), Resources.Load<Sprite> ("sprites/Skins/Omium/powered"), 1000, true, DefiantMod.Type.POWERED));
			mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/master"), Resources.Load<Sprite> ("sprites/Skins/Omium/master"), 2000, true, DefiantMod.Type.MASTER));
			mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/gold"), Resources.Load<Sprite> ("sprites/Skins/Omium/gold"), 4000, true, DefiantMod.Type.GOLD));
			defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Omium/default"), true, false, 250, Defiant.Type.OMIUM, Resources.Load<Sprite>("sprites/Skins/Omium/Default"), Defiant.SuperType.LASER, mods3, "Omium"));

			GameInformation.CurrentGame.Defiants = defiants;
			GameInformation.CurrentGame.ShowDamageText = true;

			for(int i = 0; i < GameInformation.CurrentGame.Defiants.Count; i++)
			{
				GameInformation.CurrentGame.Defiants[i].Locked = GameInformation.CurrentGame.DefiantsLockStatus[i];
				GameInformation.CurrentGame.Defiants[i].Available = GameInformation.CurrentGame.DefiantsAvailableStatus[i];

				for(int j = 0; j < GameInformation.CurrentGame.Defiants[i].Mods.Count; j++)
					GameInformation.CurrentGame.Defiants[i].Mods[j].Locked = GameInformation.CurrentGame.ModsLockStatus[i][j];
			}

			GameInformation.CurrentGame.CurrentDefiant = GameInformation.CurrentGame.Defiants[GameInformation.CurrentGame.CurrentDefiantIndex];
			GameInformation.CurrentGame.CurrentDefiantMod = GameInformation.CurrentGame.CurrentDefiant.Mods[GameInformation.CurrentGame.CurrentMods[GameInformation.CurrentGame.CurrentDefiantIndex]];
			GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab = GameInformation.CurrentGame.CurrentDefiantMod.ModPrefab;

			Debug.Log("Loading saved game");
		}
		else {
			GameInformation.CurrentGame = new Game ();
			Debug.Log("Creating new Game!");
		}
	}
}