﻿using UnityEngine;
using System.Collections;

public class LevelLoaded : MonoBehaviour {

	[SerializeField] int level;

	void Awake()
	{
		GameInformation.CurrentLevel = level;
		GameInformation.CurrentScore = 0;
	}
}
