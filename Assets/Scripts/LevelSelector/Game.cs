using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Game {

	[SerializeField] private List<Level> levels;
	[SerializeField] private List<bool> defiantsLockStatus;
	[SerializeField] private List<bool> defiantsAvailableStatus;
	[SerializeField] private List<List<bool>> modsLockStatus;
	[SerializeField] private int currentDefiantIndex;
	[SerializeField] private DefiantMod.Type currentDefiantModType;
	[SerializeField] private int coins;
	[SerializeField] private int diamonds;
	[SerializeField] private int shieldBoosts;
	[SerializeField] private int lifeSavers;
	[SerializeField] private int scoreBoosts;
	[SerializeField] private List<int> currentMods;
	[SerializeField] private int bestScore;
	[SerializeField] private bool showDamageText;
	[SerializeField] private bool autoLogin;
	[System.NonSerialized] private Defiant currentDefiant;
	[System.NonSerialized] private DefiantMod currentDefiantMod;
	public bool accelerometerControl;
	[System.NonSerialized] private List<Defiant> defiants;
	[System.NonSerialized] private Defiant.Type currentModView;

	public Game()
	{
		autoLogin = false;
		shieldBoosts = 0;
		lifeSavers = 0;
		scoreBoosts = 0;
		coins = 0;
		diamonds = 0;
		levels = new List<Level> ();
		levels.Add (new Level (1, 0, 2, false, 0));
		levels.Add (new Level (2, 0, 3, true, 50000));

		List<DefiantMod> mods = new List<DefiantMod> ();
		List<Defiant> defiants = new List<Defiant> ();
		mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/default"), Resources.Load<Sprite> ("sprites/Skins/Classic/default"), 0, false, DefiantMod.Type.DEFAULT));
		mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/powered"), Resources.Load<Sprite> ("sprites/Skins/Classic/powered"), 1000, true, DefiantMod.Type.POWERED));
		mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/master"), Resources.Load<Sprite> ("sprites/Skins/Classic/master"), 2000, true, DefiantMod.Type.MASTER));
		mods.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Classic/gold"), Resources.Load<Sprite> ("sprites/Skins/Classic/gold"), 4000, true, DefiantMod.Type.GOLD));
		defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Classic/default"), false, true, 0.0f, Defiant.Type.CLASSIC, Resources.Load<Sprite>("sprites/Skins/Classic/default"), Defiant.SuperType.BULLET, mods, "Classic"));

		List<DefiantMod> mods2 = new List<DefiantMod> ();
		mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/default"), Resources.Load<Sprite> ("sprites/Skins/Alpha/default"), 0, false, DefiantMod.Type.DEFAULT));
		mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/powered"), Resources.Load<Sprite> ("sprites/Skins/Alpha/powered"), 1000, true, DefiantMod.Type.POWERED));
		mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/master"), Resources.Load<Sprite> ("sprites/Skins/Alpha/master"), 2000, true, DefiantMod.Type.MASTER));
		mods2.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/gold"), Resources.Load<Sprite> ("sprites/Skins/Alpha/gold"), 4000, true, DefiantMod.Type.GOLD));
		defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Alpha/default"), true, false, 150, Defiant.Type.ALPHA, Resources.Load<Sprite>("sprites/Skins/Alpha/default"), Defiant.SuperType.BULLET, mods2, "Alpha"));

		List<DefiantMod> mods3 = new List<DefiantMod> ();
		mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/default"), Resources.Load<Sprite> ("sprites/Skins/Omium/default"), 0, false, DefiantMod.Type.DEFAULT));
		mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/powered"), Resources.Load<Sprite> ("sprites/Skins/Omium/powered"), 1000, true, DefiantMod.Type.POWERED));
		mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/master"), Resources.Load<Sprite> ("sprites/Skins/Omium/master"), 2000, true, DefiantMod.Type.MASTER));
		mods3.Add (new DefiantMod ((GameObject)Resources.Load("prefabs/Spaceships/Omium/gold"), Resources.Load<Sprite> ("sprites/Skins/Omium/gold"), 4000, true, DefiantMod.Type.GOLD));
		defiants.Add (new Defiant ((GameObject)Resources.Load("prefabs/Spaceships/Omium/default"), true, false, 250, Defiant.Type.OMIUM, Resources.Load<Sprite>("sprites/Skins/Omium/Default"), Defiant.SuperType.LASER, mods3, "Omium"));

		currentDefiantIndex = (int)Bullet.Type.Single;

		currentMods = new List<int> ();
		for (int i = 0; i < defiants.Count; i++)
			currentMods.Add (0);

		modsLockStatus = new List<List<bool>> ();
		List<bool> modsAux = new List<bool> ();

		for (int i = 0; i < defiants.Count; i++) {
			for(int j = 0; j < defiants[i].Mods.Count; j++)
			{
				modsAux.Add(defiants[i].Mods[j].Locked);
			}
			modsLockStatus.Add(modsAux);
		}

		defiantsLockStatus = new List<bool> ();
		defiantsAvailableStatus = new List<bool> ();
		for (int i = 0; i < defiants.Count; i++) {
			defiantsLockStatus.Add (defiants [i].Locked);
			defiantsAvailableStatus.Add (defiants [i].Available);
		}
		
		GameInformation.CurrentGame = this;
		GameInformation.CurrentGame.Defiants = defiants;
		GameInformation.CurrentGame.CurrentDefiant = defiants [currentDefiantIndex];
		GameInformation.CurrentGame.CurrentDefiantMod = currentDefiant.Mods [0];
		currentDefiant.DefiantPrefab.GetComponent<SpriteRenderer>().sprite = currentDefiant.Mods[0].ModSprite;

	}
	
	public bool SetScoreLevel( int level, int score)
	{
		for(int i = 0; i < levels.Count; ++i)
		{
			if(levels[i].Number == level)
			{
				if(levels[i].Score < score)
				{
					levels[i].Score = score;
					return true;
				}
			}
		}

		return false;
	}

	public void SetNewScore()
	{
		if (FB.IsLoggedIn) {
			FBRanking.SetScore (GameInformation.CurrentScore);
		}
	}

	public Defiant GetDefiantByType(Defiant.Type type)
	{
		Defiant defiant = defiants[0];

		for (int i = 0; i < defiants.Count; i++) {
			if(defiants[i].DefiantType == type)
				defiant = defiants[i];
		}

		return defiant;
	}


	public void SetDefiant( Defiant.Type defiant)
	{
		for(int i = 0; i < defiants.Count; ++i)
		{
			if(defiants[i].DefiantType == defiant && !defiants[i].Locked)
			{
				currentDefiant = defiants[i];
				currentDefiantIndex = (int)defiant;
			}
		}
	}

	public int GetDefiantIndexByType(Defiant.Type type)
	{
		for (int i = 0; i < defiants.Count; i++) {
			if(defiants[i].DefiantType == type)
				return i;
		}
		return 0;
	}

	public void SetDefiantMod(Defiant defiantToSet, DefiantMod.Type mod)
	{
		int index = GetDefiantIndexByType (defiantToSet.DefiantType);

		for(int i = 0; i < defiants[index].Mods.Count; ++i)
		{
			if(defiants[index].Mods[i].ModType == mod && !currentDefiant.Mods[i].Locked)
			{
				defiants[index].DefiantPrefab = currentDefiant.Mods[i].ModPrefab;
				defiants[index].DefiantSprite = currentDefiant.Mods[i].ModSprite;
				currentMods[index] = (int)mod;
			}
		}
	}

	public void UnlockLevel(int level)
	{
		for(int i = 0; i < levels.Count; i++)
		{
			if(levels[i].Number == level)
				levels[i].Locked = false;
		}
	}

	public void UnlockDefiant(Defiant.Type bullet)
	{
		for(int i = 0; i < defiants.Count; i++)
		{
			if(defiants[i].DefiantType == bullet)
				defiants[i].Locked = false;
		}
	}

	public int GetLevelScore(int level)
	{
		for(int i = 0; i < levels.Count; ++i)
		{
			if(levels[i].Number == level)
				return levels[i].Score;
		}

		return 0;
	}

	public int GetTotalScore()
	{
		int totalScore = 0;

		for (int i = 0; i < levels.Count; ++i) {
			totalScore += levels[i].Score;
		}

		return totalScore;
	}

	public bool isLockedLevel(int level)
	{
		return levels [level - 1].Locked;
	}

	public List<Level> Levels {
		get{ return levels;}
		set{ levels = value;}
	}
	public List<int> CurrentMods {
		get{ return currentMods;}
		set{ currentMods = value;}
	}
	public List<bool> DefiantsLockStatus {
		get{ return defiantsLockStatus;}
		set{ defiantsLockStatus = value;}
	}
	public List<bool> DefiantsAvailableStatus {
		get{ return defiantsAvailableStatus;}
		set{ defiantsAvailableStatus = value;}
	}
	public List<List<bool>> ModsLockStatus {
		get{ return modsLockStatus;}
		set{ modsLockStatus = value;}
	}
	public int CurrentDefiantIndex {
		get{ return currentDefiantIndex;}
		set{ currentDefiantIndex = value;}
	}
	public Defiant CurrentDefiant {
		get{ return currentDefiant;}
		set{ currentDefiant = value;}
	}
	public DefiantMod.Type CurrentDefiantModType {
		get{ return currentDefiantModType;}
		set{ currentDefiantModType = value;}
	}
	public DefiantMod CurrentDefiantMod {
		get{ return currentDefiantMod;}
		set{ currentDefiantMod = value;}
	}
	public List<Defiant> Defiants {
		get{ return defiants;}
		set{ defiants = value;}
	}
	public int Coins {
		get{ return coins; }
		set{ coins = value; }
	}
	public int Diamonds {
		get{ return diamonds; }
		set{ diamonds = value; }
	}
	public int ShieldBoosts {
		get{ return shieldBoosts; }
		set{ shieldBoosts = value; }
	}
	public int LifeSavers {
		get{ return lifeSavers; }
		set{ lifeSavers = value; }
	}
	public int ScoreBoosts {
		get{ return scoreBoosts; }
		set{ scoreBoosts = value; }
	}
	public int BestScore {
		get{ return bestScore; }
		set{ bestScore = value; }
	}
	public Defiant.Type CurrentModView {
		get{ return currentModView; }
		set{ currentModView = value; }
	}
	public bool ShowDamageText {
		get{ return showDamageText; }
		set{ showDamageText = value; }
	}

	public bool AutoLogin {
		get{ return autoLogin; }
		set{ autoLogin = value; }
	}

	public void addLevel(Level level){
		levels.Add (level);
	}

}
