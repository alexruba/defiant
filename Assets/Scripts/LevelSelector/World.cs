﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class World {

	private int number;
	private List<Level> levels;
	private int stars;

	public World()
	{
		number = 1;
		levels = new List<Level> ();
		stars = 1;
	}

	public World(int number, List<Level> levels, int stars)
	{	
		this.number = number;
		this.levels = levels;
		this.stars = stars;
	}

	public int Number {
		get{ return number;}
		set{ number = value;}
	}
	public List<Level> Levels {
		get{ return levels;}
		set{ levels = value;}
	}
	public int Stars {
		get{ return stars;}
		set{ stars = value;}
	}

}
