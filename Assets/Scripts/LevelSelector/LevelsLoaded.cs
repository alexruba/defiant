using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelsLoaded : MonoBehaviour {

	[SerializeField] Sprite lockedSprite;
	[SerializeField] Sprite unlockedSprite;
	[SerializeField] Image[] levelImage;
	[SerializeField] GameObject[] lockObjects;

	private List<Level> levels;
	private int totalScore;

	void Start()
	{
		levels = GameInformation.CurrentGame.Levels;
		totalScore = GameInformation.CurrentGame.GetTotalScore ();
		for (int i = 0; i < levels.Count; ++i) 
		{
			if(totalScore >= levels[i].PointsToUnlock)
			{
				GameInformation.CurrentGame.UnlockLevel(levels[i].Number);
			}

			if(levels[i].Locked)
			{
				levelImage[i].sprite = lockedSprite;
				lockObjects[i].SetActive(false);
			}
			else
			{
				levelImage[i].sprite = unlockedSprite;
				lockObjects[i].SetActive(true);
			}
		}
	}	
}
