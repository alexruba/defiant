﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameInformation : MonoBehaviour {

	private bool firstTime = true;
	private bool fbInitialized = false;
	private int combo;
	private int ingameCoins = 0;
	private int ingameDiamonds = 0;
	private int rangersDefeated = 0;
	private int enigmasDefeated = 0;
	private int enemiesDefeated = 0;
	private int ghostsDefeated = 0;
	private int leopardsDefeated = 0;
	private int meteorHits = 0;
	public static GameInformation Instance;

	void Awake(){
		fbInitialized = false;
		DontDestroyOnLoad (gameObject);
		if (firstTime && !Instance) {
			SaveLoad.LoadAllInformation ();
			firstTime = false;
			Instance = this;
		} else if(Instance) {
			DestroyImmediate(gameObject);
		}
	}

	public void UpdateAchievements()
	{
		if (Social.Active.localUser.authenticated) {

			// Welcome to Defiant achievement
			GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQGg");
			
			if (GameInformation.EnemiesDefeated > 0) {
				// Good Job achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQFQ", GameInformation.EnemiesDefeated);
				
				// Rampage achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQBA", GameInformation.EnemiesDefeated);
				
				// Ownage achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQBQ", GameInformation.EnemiesDefeated);
			}
			
			if (GameInformation.EnigmasDefeated > 0) {
				// Enigma come with me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQIA", GameInformation.EnigmasDefeated);
				
				// Enigma killer achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQEw", GameInformation.EnigmasDefeated);
				
				// Enigma assassin achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQFA", GameInformation.EnigmasDefeated);
			}
			
			if (GameInformation.RangersDefeated > 0) {
				// Rangers come with me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQHQ", GameInformation.RangersDefeated);
				
				// Ranger killer achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQCQ", GameInformation.RangersDefeated);
				
				// Ranger assassin achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQCg", GameInformation.RangersDefeated);
			}
			
			if (GameInformation.GhostsDefeated > 0) {
				// Ghosts come with me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQHw", GameInformation.GhostsDefeated);
				
				// Ghosts killer achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQDQ", GameInformation.GhostsDefeated);
				
				// Ghosts assassin achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQDg", GameInformation.GhostsDefeated);
			}
			
			if (GameInformation.LeopardsDefeated > 0) {
				// Leopards come with me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQHg", GameInformation.LeopardsDefeated);
				
				// Leopards killer achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQCw", GameInformation.LeopardsDefeated);
				
				// Leopards assassin achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQDA", GameInformation.LeopardsDefeated);
			}
			
			if (GameInformation.IngameCoins > 0) {
				// Coins come to me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQFg", GameInformation.IngameCoins);
				
				// The Millionaire achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQFw", GameInformation.IngameCoins);
			}
			
			if (GameInformation.IngameDiamonds > 0) {
				// Diamond Fever achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQGA", GameInformation.IngameDiamonds);
			}
			
			if (GameInformation.MeteorHits > 0) {
				// Meteor Lover achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQBw", GameInformation.MeteorHits);
				
				// Meteor, Marry me achievement
				GooglePlayServices.IncrementAchievement("CgkIxfvWvYwGEAIQCA", GameInformation.MeteorHits);
			}
		}
	}

	public static Game CurrentGame{get;set;}
	public static int CurrentLevel{get;set;}
	public static int CurrentScore{get;set;}
	public static int IngameCoins{get;set;}
	public static int IngameDiamonds{get;set;}
	public static int RangersDefeated{get;set;}
	public static int EnigmasDefeated{get;set;}
	public static int EnemiesDefeated{get;set;}
	public static int GhostsDefeated{get;set;}
	public static int LeopardsDefeated{get;set;}
	public static int MeteorHits{get;set;}
	public static bool FbInitialized{get;set;}
	public static int Combo {get;set;}



}
