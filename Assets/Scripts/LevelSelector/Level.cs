﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Level {

	private int number;
	private int score;
	private int nextLevel;
	private bool locked;
	private int pointsToUnlock;

	public Level()
	{
		number = 1;
		score = 0;
		nextLevel = 1;
		locked = true;
		pointsToUnlock = 0;
	}

	public Level(int number, int score, int nextLevel, bool locked, int pointsToUnlock)
	{
		this.number = number;
		this.score = score;
		this.nextLevel = nextLevel;
		this.locked = locked;
		this.pointsToUnlock = pointsToUnlock;
	}

	public int Number {
		get{ return number;}
		set{ number = value;}
	}
	public int Score {
		get{ return score;}
		set{ score = value;}
	}
	public int NextLevel {
		get{ return nextLevel;}
		set{ nextLevel = value;}
	}
	public bool Locked {
		get{ return locked;}
		set{ locked = value;}
	}
	public int PointsToUnlock {
		get{ return pointsToUnlock;}
		set{ pointsToUnlock = value;}
	}

}
