﻿using UnityEngine;
using System.Collections;

public class SonicExplosion : MonoBehaviour {

	public float damage;
	public float penetration;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy")) {
			Enemy enemy = other.gameObject.GetComponent<Enemy> ();
			enemy.TakeDamage (damage, penetration);
			
			if (enemy.health <= 0)
				Player.superAttackCounter += enemy.superAttackBoost;
		}

		else if (other.tag.Equals ("Player")) {
			Player player = other.gameObject.GetComponent<Player>();
			player.TakeDamage(damage, penetration);
		}
	}
}
