﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using GooglePlayGames.BasicApi;

public class GooglePlayServices : MonoBehaviour {
	
	public static GooglePlayServices Instance;
	public GameObject GPLoggedIn;
	public GameObject GPNotLoggedIn;

	void Start()
	{
		PlayGamesPlatform.Activate ();

		if (GameInformation.CurrentGame.AutoLogin)
			Login ();

		if (Social.Active.localUser.authenticated) {
			GPLoggedIn.SetActive (true);
			GPNotLoggedIn.SetActive(false);
		} else {
			GPLoggedIn.SetActive (false);
			GPNotLoggedIn.SetActive(true);
		}
	}

	public void Login()
	{
		Social.Active.localUser.Authenticate ((bool success) => {
			if(success)
			{
				GameInformation.CurrentGame.AutoLogin = true;
				SaveLoad.SaveAllInformation ();

				GPNotLoggedIn.SetActive(false);
				GPLoggedIn.SetActive(true);
			}
			else
			{
				GPNotLoggedIn.SetActive(true);
				GPLoggedIn.SetActive(false);
			}
		});
	}
	
	public void ViewAchievements()
	{
		// show achievements UI
		Social.ShowAchievementsUI();
	}

	public static void IncrementAchievement(string achievementCode, int increment)
	{
		if (!PlayGamesPlatform.Instance.GetAchievement(achievementCode).IsUnlocked) {
			PlayGamesPlatform.Instance.IncrementAchievement(
				achievementCode, increment, (bool success) => {
				// handle success or failure
				if (success && PlayGamesPlatform.Instance.GetAchievement (achievementCode).IsUnlocked) {
					GameInformation.CurrentGame.Diamonds += 5;
					SaveLoad.SaveAllInformation ();
				}
			});
		}
	}

	public static void UnlockAchievement(string achievementCode)
	{
		if (!PlayGamesPlatform.Instance.GetAchievement(achievementCode).IsUnlocked) {
			// unlock achievement
			Social.ReportProgress(achievementCode, 100.0f, (bool success) => {
				// handle success or failure
				if(success)
				{
					GameInformation.CurrentGame.Diamonds += 5;
					SaveLoad.SaveAllInformation ();
				}
			});
		}
	}

	public bool Authenticated {
		get {
			return Social.Active.localUser.authenticated;
		}
	}
}
