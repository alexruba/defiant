﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FBHolder : MonoBehaviour {

	public GameObject UIFBIsLoggedIn;
	public GameObject UIFBNotLoggedIn;
	public GameObject UIFBAvatar;
	public GameObject UIFBUserName;
	private bool firstTime;

	public Text ScoresDebug;
	public List<object> scoresList = null;

	private Dictionary<string, string> profile = null;

	void Awake()
	{
		if (!GameInformation.FbInitialized) {
			GameInformation.FbInitialized = true;
			FB.Init (SetInit, OnHideUnity);
		}

		if (!FB.IsLoggedIn) {
			DealWithFBMenus(false);
		}
		else {
			DealWithFBMenus(true);
		}
	}

	private void SetInit()
	{
		if (FB.IsLoggedIn) {
			Debug.Log ("FB init done and logged :D!");
			DealWithFBMenus(true);
		} else {
			Debug.Log ("FB not logged :(");
			DealWithFBMenus(false);
		}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}

	public void FBLogin()
	{
		FB.Login ("email, publish_actions, user_friends", AuthCallback);
	}

	void AuthCallback(FBResult result)
	{
		if (FB.IsLoggedIn) {
			Debug.Log ("FB login worked!");
			DealWithFBMenus(true);
		} else {
			Debug.Log ("FB login failed :(");
			DealWithFBMenus(false);
		}
	}

	void DealWithFBMenus(bool isLoggedIn)
	{
		if (isLoggedIn) {
			UIFBIsLoggedIn.SetActive(true);
			UIFBNotLoggedIn.SetActive(false);

			// get profile picture code
			FB.API(Util.GetPictureURL("me", 128, 128), Facebook.HttpMethod.GET, DealWithProfilePicture);

			// get username code
			FB.API("/me?fields=id,first_name", Facebook.HttpMethod.GET, DealWithUserName);
		} else {
			UIFBIsLoggedIn.SetActive(false);
			UIFBNotLoggedIn.SetActive(true);		
		}
	}

	void DealWithProfilePicture(FBResult result)
	{
		if (result.Error != null) {
			Debug.Log ("An error ocurred when getting profile picture");
			FB.API (Util.GetPictureURL ("me", 128, 128), Facebook.HttpMethod.GET, DealWithProfilePicture);

			return;
		} else {
			Image UserAvatar = UIFBAvatar.GetComponent<Image>();
			UserAvatar.sprite = Sprite.Create(result.Texture, new Rect(0,0,128,128), new Vector2(0,0));
		}
	}

	void DealWithUserName(FBResult result)
	{
		if (result.Error != null) {
			Debug.Log ("An error ocurred when getting user name");
			FB.API("/me?fields=id,first_name", Facebook.HttpMethod.GET, DealWithUserName);

			return;
		} else {
			profile = Util.DeserializeJSONProfile(result.Text);

			Text UserMsg = UIFBUserName.GetComponent<Text>();
			UserMsg.text = "Hello, " + profile["first_name"];
		}
	}

	public void ShareWithFriends()
	{
		FB.Feed (
			linkCaption: "I'm playing Defiant now!",
			picture: "http://greyzoned.com/images/evilelef2_icon.png",
			linkName: "Check out this awesome game",
			link: "https://play.google.com/apps/testing/com.centrix.defiant"
		);
	}

	public void InviteFriends()
	{
		FB.AppRequest (
			message: "This game is awesome, join me, now!",
			title: "Invite your friends to join you"
		);
	}

}

























