﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FBRanking : MonoBehaviour {

	public List<object> scoresList = null;
	public GameObject ScoreEntryPanel;
	public GameObject ScoresScrollList;

	// All Scores API related Things
	void Start()
	{
		if (FB.IsLoggedIn) {
			QueryScores();
		}
	}
	public void QueryScores()
	{
		FB.API ("/app/scores?fields=score,user.limit(30)", Facebook.HttpMethod.GET, ScoresCallback);
	}
	
	private void ScoresCallback(FBResult result)
	{
		Debug.Log ("Scores callback: " + result.Text);

		scoresList = Util.DeserializeScores (result.Text);
		
		foreach (object score in scoresList) 
		{
			var entry = (Dictionary<string, object>) score;
			var user = (Dictionary<string, object>) entry["user"];

			GameObject ScorePanel;
			ScorePanel = Instantiate(ScoreEntryPanel) as GameObject;
			ScorePanel.transform.SetParent(ScoresScrollList.transform);
			if(Application.platform == RuntimePlatform.Android)
				ScorePanel.transform.localScale = new Vector3(0.8f, 0.8f, 0.5f);
			else if(DeviceType.Desktop == SystemInfo.deviceType)
				ScorePanel.transform.localScale = new Vector3(0.7f, 0.7f, 0.5f);

			Transform ThisScoreName = ScorePanel.transform.Find("FriendName");
			Transform ThisScoreScore = ScorePanel.transform.Find("FriendScore");

			Text ScoreName = ThisScoreName.GetComponent<Text>();
			Text ScoreScore = ThisScoreScore.GetComponent<Text>();

			ScoreName.text = user["name"].ToString();
			ScoreScore.text = entry["score"].ToString();

			Transform TheUserAvatar = ScorePanel.transform.Find("FriendAvatar");
			Image UserAvatar = TheUserAvatar.GetComponent<Image>();

			FB.API(Util.GetPictureURL(user["id"].ToString(), 128, 128), Facebook.HttpMethod.GET, delegate(FBResult pictureResult) {
				if(pictureResult.Error != null)
				{
					Debug.Log ("An error ocurred when getting the friend image");
				}
				else
				{
					UserAvatar.sprite = Sprite.Create(pictureResult.Texture, new Rect(0,0,128,128), new Vector2(0,0));
				}
			});

		}
	}
	
	public static void SetScore(int score)
	{
		var scoreData = new Dictionary<string,string> ();
		scoreData ["score"] = score.ToString ();
		FB.API ("/me/scores", Facebook.HttpMethod.POST, delegate(FBResult result) {
			Debug.Log ("Score submit result: " + result.Text);
		}, scoreData);
	}
}
