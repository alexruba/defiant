﻿using UnityEngine;
using System.Collections;

public class SuperDrop : Drop {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Player")) {
			Player.superAttackCounter += 10;
			Destroy (gameObject);
		}
	}
}
