﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropManager : MonoBehaviour {

	public GameObject[] drops;

	public Drop GetDrop()
	{
		float total = 0;
		
		foreach (GameObject elem in drops) {
			total += elem.GetComponent<Drop>().probability;
		}
		
		float randomPoint = Random.value * total;
		
		foreach (GameObject elem in drops) {
			if (randomPoint < elem.GetComponent<Drop>().probability) {
				return elem.GetComponent<Drop>();
			}
			else {
				randomPoint -= elem.GetComponent<Drop>().probability;
			}
		}

		return drops[drops.Length - 1].GetComponent<Drop>();
	}
}
