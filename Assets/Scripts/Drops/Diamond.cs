﻿using UnityEngine;
using System.Collections;

public class Diamond : Drop {

	public GameObject diamondEffect;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Player")) {
			GameObject.Find ("CoinClip").GetComponent<AudioSource>().Play();
		
			GameInformation.CurrentGame.Diamonds += 1;
			GameInformation.IngameDiamonds += 1;
			Destroy(Instantiate(diamondEffect, transform.position, Quaternion.identity), 0.5f);

			if(GameInformation.IngameDiamonds >= 10)
				// More Diamonds please achievement
				GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQGw");

			gameObject.SetActive(false);
			Destroy (gameObject, 1f);
		}
	}
}
