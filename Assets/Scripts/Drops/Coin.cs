﻿using UnityEngine;
using System.Collections;

public class Coin : Drop {

	public GameObject coinEffect;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Player")) {
			GameObject.Find ("CoinClip").GetComponent<AudioSource>().Play();
			if(GameInformation.CurrentGame.CurrentDefiantModType == DefiantMod.Type.GOLD)
			{
				GameInformation.CurrentGame.Coins += 2;
				GameInformation.IngameCoins += 2;
			} else
			{
				GameInformation.CurrentGame.Coins += 1;
				GameInformation.IngameCoins += 1;
			}

			Destroy(Instantiate(coinEffect, transform.position, Quaternion.identity), 0.5f);

			gameObject.SetActive(false);
			Destroy (gameObject, 1f);
		}
	}
}
