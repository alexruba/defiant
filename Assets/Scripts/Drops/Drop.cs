﻿using UnityEngine;
using System.Collections;

public class Drop : MonoBehaviour {

	public enum Type{ COIN, NOTHING, DIAMOND, BOMB, SUPERBOOST, HEALTH }
	public Type dropType;
	public GameObject dropObject;
	public float probability;

	void Start()
	{
		Destroy (gameObject, 5f);
	}
}
