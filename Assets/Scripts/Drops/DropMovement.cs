﻿using UnityEngine;
using System.Collections;

public class DropMovement : MonoBehaviour {

	public float speed;

	void FixedUpdate()
	{
		Vector2 pos = transform.position;
		pos += new Vector2(0, -1f) 
			* speed * 10 * Time.deltaTime;
		
		transform.position = pos;
	}
}
