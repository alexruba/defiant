﻿using UnityEngine;
using System.Collections;

public class Health : Drop {

	public float health;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Player")) {
			GameObject.Find ("CoinClip").GetComponent<AudioSource>().Play();
			other.gameObject.GetComponent<Player>().IncreaseHealth(health);
			Destroy (gameObject);
		}
	}
}
