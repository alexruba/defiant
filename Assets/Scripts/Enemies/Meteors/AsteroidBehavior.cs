﻿using UnityEngine;
using System.Collections;

public class AsteroidBehavior : MonoBehaviour {

	public float speed = 1;
	public float damage = 10;
	public float penetration = 0.5f;
	
	void Update()
	{
		transform.Translate (0, speed * Time.deltaTime, 0);
		transform.Rotate ( new Vector3 (0, 0, Random.Range(-40, 40) * Time.deltaTime));
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Player")) {
			GameInformation.MeteorHits++;

			other.gameObject.GetComponent<Player> ().TakeDamage (damage, penetration);
			gameObject.SetActive (false);
		} else if (other.tag.Equals ("Enemy")) {
			other.gameObject.GetComponent<Enemy> ().TakeDamage (damage / 2, penetration);
			gameObject.SetActive (false);
		}
	}
}
