﻿using UnityEngine;
using System.Collections;

public class Wave : MonoBehaviour {

	public float prob;
	public bool isSingle;

	void Update()
	{
		if (transform.childCount == 0) {
			WaveManager.wavesDefeated++;
			if(!isSingle)
				Destroy(gameObject);
		}
	}
}
