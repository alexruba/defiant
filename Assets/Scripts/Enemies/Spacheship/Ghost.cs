﻿using UnityEngine;
using System.Collections;

public class Ghost : Enemy {

	public float disappearTime;
	private GameObject player;

	void Start()
	{
		player = GameObject.FindWithTag ("Player");
		StartCoroutine ("GhostBehaviour");
	}

	IEnumerator GhostBehaviour()
	{
		while (true) {
			yield return new WaitForSeconds (disappearTime);
			DeActivateObjectAndChildren (gameObject);
			gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			yield return new WaitForSeconds (disappearTime);
			transform.Translate(player.transform.position.x - transform.position.x, 0f, 0f);
			gameObject.GetComponent<CircleCollider2D> ().enabled = true;
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			ActivateObjectAndChildren (gameObject);
		}
	}

	void FixedUpdate()
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, 1f, 1 << layerMask);
		
		if (colliders.Length != 0) {
			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0,0));
			Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1,1));
			
			Vector2 pos = transform.position;
			
			if(transform.position.x > colliders[0].transform.position.x)
				pos += new Vector2(1f, 0) 
					* speed * 10 * Time.deltaTime;
			else
				pos += new Vector2(-1f, 0) 
					* speed * 10 * Time.deltaTime;
			
			pos.x = Mathf.Clamp (pos.x, min.x, max.x);
			pos.y = Mathf.Clamp (pos.y, min.y, max.y);
			
			transform.position = pos;
		}
		else
		{
			if(transform.position.x > Camera.main.transform.position.x / 2f - 0.5f &&
			   transform.position.x < Camera.main.transform.position.x / 2f + 0.5f)
				transform.Translate (0f, -speed * Time.deltaTime, 0);
			else if(transform.position.x < Camera.main.transform.position.x / 2f - 0.5f)
				transform.Translate (0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			else
				transform.Translate (-0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			
		}
	}

	public void DeActivateObjectAndChildren(GameObject obj)
	{
		for (int i = 0; i < obj.transform.childCount; ++i)
		{
			obj.transform.GetChild(i).gameObject.SetActive(false);
		}
		
		obj.SetActive(true);
	}

	void OnDestroy()
	{
		GameInformation.GhostsDefeated++;
	}
}
