﻿using UnityEngine;
using System.Collections;

public class Enemy : Spaceship {

	public LayerMask layerMask;
	public int superAttackBoost;
	public int scoreBoost = 10;

	public void TakeDamage(float damage, float penetration)
	{
		if (shield <= 0)
			health -= damage;
		else {
			health -= penetration * damage;
			shield -= damage;
		}

		if(health <= 0)
		{
			GameObject.Find ("ExplosionClip").GetComponent<AudioSource>().Play();

			GameInformation.Combo ++;

			if(GameInformation.Combo >= 100 && Social.Active.localUser.authenticated)
			{
				// The PRO achievement
				GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQGQ");
			}

			Player.superAttackCounter += GameInformation.Combo * 0.0075f;

			GameInformation.CurrentScore += scoreBoost;
			Drop drop = GameObject.Find ("DropManager").GetComponent<DropManager>().GetDrop();
			if(drop.dropType != Drop.Type.NOTHING)
				Instantiate(drop, transform.position, Quaternion.identity);

			GameInformation.EnemiesDefeated++;

			DestroySpaceship();
		}
	}
}
