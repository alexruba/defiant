﻿using UnityEngine;
using System.Collections;

public class UfoBullets : MonoBehaviour {

	public GameObject bullet;
	public float shootingRate;
	
	void Start() 
	{
		InvokeRepeating ("Fire", 0.5f, shootingRate);
	}
	
	void Fire()
	{
		GameObject obj = ObjectPoolScript.current.GetObject (bullet);
		if (obj == null) return;
		
		obj.transform.position = transform.position;
		obj.transform.rotation = transform.rotation;
		
		ActivateObjectAndChildren(obj);
	}

	public void ActivateObjectAndChildren(GameObject obj)
	{
		for (int i = 0; i < obj.transform.childCount; ++i)
		{
			obj.transform.GetChild(i).gameObject.SetActive(true);
		}
		
		obj.SetActive(true);
	}
}
