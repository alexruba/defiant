﻿using UnityEngine;
using System.Collections;

public class Ranger : Enemy {

	public GameObject bullet;

	void Start() 
	{
		InvokeRepeating ("Fire", 0.5f, shootingRate);
	}
	
	void Fire()
	{
		GameObject obj = ObjectPoolScript.current.GetObject (bullet);
		if (obj == null) return;
		
		obj.transform.position = transform.position;
		obj.transform.rotation = transform.rotation;

		ActivateObjectAndChildren(obj);
	}

	void FixedUpdate()
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, 1f, 1 << layerMask);
		
		if (colliders.Length != 0) {
			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0,0));
			Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1,1));
			
			Vector2 pos = transform.position;
			
			if(transform.position.x > colliders[0].transform.position.x)
				pos += new Vector2(1f, 0) 
					* speed * 10 * Time.deltaTime;
			else
				pos += new Vector2(-1f, 0) 
					* speed * 10 * Time.deltaTime;
			
			pos.x = Mathf.Clamp (pos.x, min.x, max.x);
			pos.y = Mathf.Clamp (pos.y, min.y, max.y);
			
			transform.position = pos;
		}
		else
		{
			if(transform.position.x > Camera.main.transform.position.x / 2f - 0.2f &&
			   transform.position.x < Camera.main.transform.position.x / 2f + 0.2f)
				transform.Translate (0f, -speed * Time.deltaTime, 0);
			else if(transform.position.x < Camera.main.transform.position.x / 2f - 0.2f)
				transform.Translate (0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			else
				transform.Translate (-0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			
		}
	}

	void OnDestroy()
	{
		GameInformation.RangersDefeated++;
	}
}
