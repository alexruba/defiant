﻿using UnityEngine;
using System.Collections;

public class Enigma : Enemy {
	
	void FixedUpdate()
	{
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, 1f, 1 << layerMask);
		
		if (colliders.Length != 0) {
			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0,0));
			Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1,1));
			
			Vector2 pos = transform.position;
			
			if(transform.position.x > colliders[0].transform.position.x)
				pos += new Vector2(1f, 0) 
					* speed * 10 * Time.deltaTime;
			else
				pos += new Vector2(-1f, 0) 
					* speed * 10 * Time.deltaTime;
			
			pos.x = Mathf.Clamp (pos.x, min.x, max.x);
			pos.y = Mathf.Clamp (pos.y, min.y, max.y);
			
			transform.position = pos;
		}
		else
		{
			if(transform.position.x > Camera.main.transform.position.x / 2f - 0.2f &&
			   transform.position.x < Camera.main.transform.position.x / 2f + 0.2f)
				transform.Translate (0f, -speed * Time.deltaTime, 0);
			else if(transform.position.x < Camera.main.transform.position.x / 2f - 0.2f)
				transform.Translate (0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			else
				transform.Translate (-0.2f * Time.deltaTime, -speed * Time.deltaTime, 0);
			
		}
	}
	public GameObject[] enemies;
	public int numberOfEnemies;
	public GameObject[] drops;
	public int numberOfDrops;

	void OnDestroy()
	{
		int dropType = Random.Range (0, 10);
		int numberOf;

		if (dropType > 7) {
			int index = Random.Range (0, enemies.Length);
			numberOf = Random.Range (0, numberOfEnemies);
			
			for (int i = 0; i < numberOf; i++) {
				int randomX = Random.Range (-1, 1);
				int randomY = Random.Range (-1, 1);
				
				Destroy (Instantiate (enemies [index], new Vector3 (transform.position.x + randomX, transform.position.y + randomY, transform.position.z), Quaternion.identity), 3);
			}
		} else {
			numberOf = Random.Range (0, numberOfDrops);
			DropManager dropManager = GameObject.Find ("DropManager").GetComponent<DropManager>();

			for (int i = 0; i < numberOf; i++) {
				float randomX = Random.Range (-1.5f, 1.5f);
				float randomY = Random.Range (-1.5f, 1.5f);
				Drop drop = dropManager.GetDrop();
				if(drop.dropType != Drop.Type.NOTHING)
					Destroy(Instantiate(drop, new Vector3 (transform.position.x + randomX, transform.position.y + randomY, transform.position.z), Quaternion.identity), 3);			
			}
		}

		GameInformation.EnigmasDefeated++;
	}
}
