﻿using UnityEngine;
using System.Collections;

public class Lightning : Enemy {

	private GameObject player;
	public float startAttack;
	public GameObject[] lasers;
	public float laserAlive;

	void Start()
	{
		player = GameObject.FindWithTag ("Player");
		StartCoroutine ("LightningBehaviour");
	}

	IEnumerator LightningBehaviour()
	{
		StopCoroutine ("Fire");
		yield return new WaitForSeconds(startAttack);
		lasers[0].SetActive (true);
		lasers[1].SetActive (true);
		yield return new WaitForSeconds(laserAlive);
		lasers[0].SetActive (false);
		lasers[1].SetActive (false);
		StartCoroutine ("Fire");
	}

	IEnumerator Fire()
	{
		StopCoroutine ("LightningBehaviour");
		yield return new WaitForSeconds (1);
		ActivateObjectAndChildren (gameObject);
		yield return new WaitForSeconds (10);
		StartCoroutine ("LightningBehaviour");
	}
	
	void FixedUpdate()
	{
		transform.Translate ((player.transform.position.x - transform.position.x) * speed * 50 * Time.deltaTime, -speed * Time.deltaTime, 0);
	}
}
