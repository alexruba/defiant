﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComboTrack : MonoBehaviour {

	public Text comboText;

	void Update()
	{
		comboText.text = GameInformation.Combo.ToString();
	}
}
