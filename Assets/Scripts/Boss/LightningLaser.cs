﻿using UnityEngine;
using System.Collections;

public class LightningLaser : MonoBehaviour {

	public float damage;
	public float penetration;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals ("Player")) {
			Player player = other.gameObject.GetComponent<Player> ();
			player.TakeDamage (damage, penetration);			
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals ("Player")) {
			Player player = other.gameObject.GetComponent<Player> ();
			player.TakeDamage (damage, penetration);			
		}
	}
}
