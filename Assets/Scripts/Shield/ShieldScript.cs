using UnityEngine;
using System.Collections;

public class ShieldScript : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		if (transform.parent.gameObject.GetComponent<Player>().shield <= 0) {
			gameObject.SetActive(false);
		}
	}
}
