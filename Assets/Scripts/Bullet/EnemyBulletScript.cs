﻿using UnityEngine;
using System.Collections;

public class EnemyBulletScript : MonoBehaviour {

	public float damage = 10;
	public float penetration = 0.5f;
	public GameObject collisionEffect;
	
	void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.tag.Equals ("Player")) {
			Player player = other.gameObject.GetComponent<Player> ();
			player.TakeDamage (damage, penetration);
		} else if (other.tag.Equals ("Ally")) {
			Ally ally = other.gameObject.GetComponent<Ally>();
			ally.TakeDamage(damage, penetration);
		}
		if(!other.tag.Equals("Enemy") && !other.tag.Equals("Unbulleted"))
		{
			GameObject effect = ObjectPoolScript.current.GetObject (collisionEffect);
			if (effect == null) return;
			effect.transform.position = transform.position;
			
			effect.SetActive (true);
			gameObject.transform.parent.gameObject.SetActive(false);
		}

	}
}
