using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletDamageScript : MonoBehaviour {

	public GameObject collisionEffect;
	public GameObject explosion;
	public GameObject damageText;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(!other.tag.Equals("Player") && !other.tag.Equals("Unbulleted"))
		{
			GameObject effect = ObjectPoolScript.current.GetObject (collisionEffect);
			if (effect == null) return;
			effect.transform.position = transform.position;
			
			effect.SetActive (true);
			gameObject.transform.parent.gameObject.SetActive(false);
		}
		
		if (other.tag.Equals ("Enemy")) 
		{
			Enemy enemy = other.gameObject.GetComponent<Enemy> ();
			float damage = Random.Range(GameInformation.CurrentGame.CurrentDefiant.Stats.damage - 5.0f, GameInformation.CurrentGame.CurrentDefiant.Stats.damage + 5.0f);
			enemy.TakeDamage (damage, GameInformation.CurrentGame.CurrentDefiant.Stats.penetration);

			transform.parent.gameObject.SetActive (false);
			if (enemy.health <= 0)
				Player.superAttackCounter += enemy.superAttackBoost;
			
		} else if (other.name.Equals ("AsteroidLittle")) {
			
			GameObject collision = ObjectPoolScript.current.GetObject (collisionEffect);
			if (collision == null) return;
			
			collision.transform.position = other.gameObject.transform.position;
			collision.transform.rotation = other.gameObject.transform.rotation;
			
			collision.SetActive (true);
		}
		
	}

}
