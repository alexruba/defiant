﻿using UnityEngine;
using System.Collections;

public class EnemyBulletMovementScript : MonoBehaviour {

	public float speed = 5;
	
	void Update()
	{
		transform.Translate (new Vector2(0, -speed * Time.deltaTime));
	}
	
	void OnDisable()
	{
		transform.position = transform.parent.position;
	}
}
