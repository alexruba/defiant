using UnityEngine;
using System.Collections;

public class LaserLevel : MonoBehaviour {
	
	public float damage;
	public float penetration;
	public float lifetime;
	
	void Start()
	{
		Destroy (gameObject, lifetime);
		
	}
	
	void OnTriggerStay2D(Collider2D other)
	{
		if(other.gameObject.tag.Equals("Player"))
		{
			other.gameObject.GetComponent<Player>().TakeDamage(damage, penetration);
		}
		
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag.Equals("Player"))
		{
			other.gameObject.GetComponent<Player>().TakeDamage(damage, penetration);
		}
		
	}
}

