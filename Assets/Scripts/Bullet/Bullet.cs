﻿using UnityEngine;
using System.Collections;

public class Bullet {

	public enum Type{Single, Double, Trilinear, Tridirectional, DoublePowered};
	private Type bulletType;
	private GameObject bulletGameObject;
	private float damage;
	private float penetration;
	private GameObject collisionEffect;
	private bool locked;
	private bool available;

	public Bullet(Type type, GameObject bulletGameObject, float damage, float penetration, GameObject collisionEffect, bool locked, bool available)
	{
		this.bulletType = type;
		this.bulletGameObject = bulletGameObject;
		this.damage = damage;
		this.penetration = penetration;
		this.collisionEffect = collisionEffect;
		this.locked = locked;
		this.available = available;
	}

	public Type BulletType {
		get{ return bulletType; }
		set{ bulletType = value; }
	}
	public GameObject BulletGameObject {
		get{ return bulletGameObject;}
		set{ bulletGameObject = value;}
	}
	public float Damage {
		get{ return damage;}
		set{ damage = value;}
	}
	public float Penetration {
		get{ return penetration;}
		set{ penetration = value;}
	}
	public GameObject CollisionEffect {
		get{ return collisionEffect;}
		set{ collisionEffect = value;}
	}
	public bool Locked {
		get{ return locked;}
		set{ locked = value;}
	}
	public bool Available {
		get{ return available;}
		set{ available = value;}
	}
}
