﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	public GameObject bullet;
	public float shootingRate;
	public float startTime;
	
	void Start() 
	{
		StartCoroutine ("Fire");
	}
	
	IEnumerator Fire()
	{
		yield return new WaitForSeconds (startTime);
		while (true) {
			GameObject obj = ObjectPoolScript.current.GetObject (bullet);
			if (obj == null) yield return null;
			
			obj.transform.position = transform.position;
			obj.transform.rotation = transform.rotation;
			
			ActivateObjectAndChildren(obj);
			yield return new WaitForSeconds(shootingRate + startTime);
		}
	}

	public void ActivateObjectAndChildren(GameObject obj)
	{
		for (int i = 0; i < obj.transform.childCount; ++i)
		{
			obj.transform.GetChild(i).gameObject.SetActive(true);
		}
		
		obj.SetActive(true);
	}
}
