using UnityEngine;
using System.Collections;

public class TouchToStartLevel : MonoBehaviour {

	public GameData gameData;

	public GameObject[] toHide;
	public GameObject[] toShow;
	public AudioClip clip;

	public void OnTouchStart()
	{
		foreach (GameObject gh in toHide)
			gh.SetActive (false);

		foreach (GameObject gs in toShow)
			gs.SetActive (true);

		// Game parameters initialization
		GameInformation.IngameCoins = 0;
		GameInformation.IngameDiamonds = 0;
		GameInformation.MeteorHits = 0;
		GameInformation.EnemiesDefeated = 0;
		GameInformation.EnigmasDefeated = 0;
		GameInformation.GhostsDefeated = 0;
		GameInformation.LeopardsDefeated = 0;
		GameInformation.RangersDefeated = 0;

		GameData.gameStarted = true;
		gameData.StartScoreCount ();
		GameObject.Find ("Load").GetComponent<AudioSource>().Play();
		MusicSingleton.instance.GetComponent<AudioSource>().Stop ();
		MusicSingleton.instance.GetComponent<AudioSource>().clip = clip;
		MusicSingleton.instance.GetComponent<AudioSource>().Play ();

		// Set Defiant Prefab to play with
		GameInformation.CurrentGame.CurrentDefiantModType = 
			GameInformation.CurrentGame.Defiants [GameInformation.CurrentGame.CurrentDefiantIndex].Mods [GameInformation.CurrentGame.CurrentMods [GameInformation.CurrentGame.CurrentDefiantIndex]].ModType;
		Instantiate (GameInformation.CurrentGame.Defiants[GameInformation.CurrentGame.CurrentDefiantIndex].Mods[GameInformation.CurrentGame.CurrentMods[GameInformation.CurrentGame.CurrentDefiantIndex]].ModPrefab);
	}
}
