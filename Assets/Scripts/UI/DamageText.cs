﻿using UnityEngine;
using System.Collections;

public class DamageText : MonoBehaviour {

	void OnEnable()
	{
		StartCoroutine ("Disappear");
	}

	IEnumerator Disappear()
	{
		yield return new WaitForSeconds (1);
		gameObject.SetActive (false);
	}
}
