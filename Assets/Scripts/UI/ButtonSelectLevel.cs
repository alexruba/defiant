using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonSelectLevel : MonoBehaviour {

	public GameObject[] levelPanels;
	public GameObject nextButton;
	public GameObject prevButton;
	public static int currentIndex;
	[SerializeField] Text pointsToUnlockText;
	private bool ready;

	void Start()
	{
		currentIndex = 0;
		ready = true;

		levelPanels [0].SetActive (true);
		for (int i = 1; i < levelPanels.Length; i++) {
			levelPanels[i].SetActive(false);
		}
	}

	void Update()
	{
		if (Input.touchCount > 0 &&
		    Input.GetTouch(0).phase == TouchPhase.Moved && ready) 
		{
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			
			if(touchDeltaPosition.x > 1.5f)
			{
				if(currentIndex > 0)
				{
					ready = false;
					OnPrevClick();
				}
			}
			else if(touchDeltaPosition.x < -1.5f)
			{
				if(currentIndex < levelPanels.Length - 1)
				{
					ready = false;
					OnNextClick();
				}
			}
		}

		if (currentIndex == levelPanels.Length - 1)
			nextButton.SetActive (false);
		else
			nextButton.SetActive (true);

		if (currentIndex > 0)
			prevButton.SetActive (true);
		else
			prevButton.SetActive (false);
	}

	public void OnNextClick()
	{
		levelPanels [currentIndex].GetComponent<Animator> ().SetTrigger ("LeaveScreen");
		StartCoroutine (WaitForNextAnimation (0.4f));
	}

	public void OnPrevClick()
	{
		levelPanels [currentIndex].GetComponent<Animator> ().SetTrigger ("Disappear");
		StartCoroutine (WaitForPrevAnimation (0.25f));
	}

	IEnumerator WaitForNextAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		if(currentIndex < levelPanels.Length - 1)
			currentIndex ++;
		if(levelPanels[currentIndex].activeInHierarchy) levelPanels [currentIndex].GetComponent<Animator> ().SetTrigger ("Next");
		
		if (GameInformation.CurrentGame.Levels [currentIndex].Locked) {
			pointsToUnlockText.text = "" + GameInformation.CurrentGame.Levels [currentIndex].PointsToUnlock;
			pointsToUnlockText.gameObject.SetActive (true);
		}
		else
			pointsToUnlockText.gameObject.SetActive (false);
		
		levelPanels [currentIndex].SetActive (true);
		ready = true;
	}

	IEnumerator WaitForPrevAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		
		if (currentIndex > 0)
			currentIndex --;
		levelPanels [currentIndex].GetComponent<Animator> ().SetTrigger ("Prev");
		
		if (GameInformation.CurrentGame.Levels [currentIndex].Locked) {
			pointsToUnlockText.text = "" + GameInformation.CurrentGame.Levels [currentIndex].PointsToUnlock;
			pointsToUnlockText.gameObject.SetActive (true);
		}
		else
			pointsToUnlockText.gameObject.SetActive (false);

		ready = true;
	}
}
