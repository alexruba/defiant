using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoreManager : MonoBehaviour {

	private List<GameObject> itemsPanel;
	public GameObject[] itemsPanelObject;
	public Transform[] panelTransforms;
	public Transform panelTransform;
	public GameObject itemPanel;
	public GameObject nextButton;
	public GameObject prevButton;
	public static int currentIndex;
	private bool ready;
	public BuyPopup popup;
	public ItemPanel item;
	public BuyButton buyButton;
	private List<Defiant> defiants;
	private Defiant currentDefiant;
	public Text emptyText;
	private int currentPanel;
	private int currentBoost;
	private int defiantNum = 0;

	void Start()
	{
		currentIndex = 0;
		ready = true;
		itemsPanel = new List<GameObject> ();
		defiants = new List<Defiant> ();

		for (int i = 0; i < itemsPanelObject.Length; i++) {
			itemsPanel.Add(itemsPanelObject[i]);

			if(i == 0)
			{
				for (int j = 0; j < GameInformation.CurrentGame.Defiants.Count; j++) {
					if (!GameInformation.CurrentGame.Defiants [j].Available) 
					{
						item.SetText(GameInformation.CurrentGame.Defiants[j].DefiantType.ToString());
						item.SetImage(GameInformation.CurrentGame.Defiants[j].DefiantSprite);
						item.SetButton(GameInformation.CurrentGame.Defiants[j].Price, 0, (int)GameInformation.CurrentGame.Defiants[j].DefiantType);
						GameObject newPanel = (GameObject)Instantiate (itemPanel);
						newPanel.name = GameInformation.CurrentGame.Defiants [j].DefiantPrefab.name;
						if(SystemInfo.deviceType == DeviceType.Desktop)
							newPanel.transform.localScale = new Vector3(0.6f, 0.6f, 0.5f);
						else if(Application.platform == RuntimePlatform.Android)
							newPanel.transform.localScale = new Vector3(0.7f, 0.7f, 0.5f);
						else
							newPanel.transform.localScale = new Vector3(0.95f, 0.95f, 0.5f);

						newPanel.transform.SetParent (itemsPanel[i].GetComponent<GridLayoutGroup>().transform);
						defiantNum++;
						defiants.Add(GameInformation.CurrentGame.Defiants[j]);
					}
				}
			}
		}
		if (defiantNum == 0)
			emptyText.gameObject.SetActive(true);

		itemsPanel[0].SetActive (true);

		for (int i = 0; i < itemsPanel[0].GetComponentsInChildren<Button>().Length; i++) {
			int counter = i;
			itemsPanel[0].GetComponentsInChildren<Button> ()[i].onClick.AddListener (() => OnItemClick(defiants[counter], counter));
		}			
	}

	void Update()
	{
		if (Input.touchCount > 0 &&
		    Input.GetTouch(0).phase == TouchPhase.Moved && ready) 
		{
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			
			if(touchDeltaPosition.x > 1.5f)
			{
				if(currentIndex > 0)
				{
					ready = false;
					OnPrevClick();
				}
			}
			else if(touchDeltaPosition.x < -1.5f)
			{
				if(currentIndex < itemsPanel.Count - 1)
				{
					ready = false;
					OnNextClick();
				}
			}
		}

		if (currentIndex == itemsPanel.Count - 1)
			nextButton.SetActive (false);
		else
			nextButton.SetActive (true);

		if (currentIndex > 0)
			prevButton.SetActive (true);
		else
			prevButton.SetActive (false);
	}

	public void OnNextClick()
	{
		itemsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("LeaveScreen");
		StartCoroutine (WaitForNextAnimation (0.4f));
	}

	public void OnPrevClick()
	{
		itemsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Disappear");
		StartCoroutine (WaitForPrevAnimation (0.25f));
	}

	IEnumerator WaitForNextAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		if(currentIndex < itemsPanel.Count - 1)
			currentIndex ++;

		if(itemsPanel[currentIndex].activeInHierarchy) itemsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Next");

		itemsPanel [currentIndex].SetActive (true);

		ready = true;
	}

	IEnumerator WaitForPrevAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		
		if (currentIndex > 0)
			currentIndex --;

		itemsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Prev");

		ready = true;
	}

	public void OnItemClick(Defiant defiant, int panel)
	{
		currentDefiant = defiant;
		currentPanel = panel;
		popup.Pop (defiant.DefiantType.ToString(), (int)defiant.Price);

	}

	public void OnItemBuy()
	{
		if (currentIndex == 0) {
			OnDefiantBuy ();
		} else {
			OnBoostBuy();
		}
	}

	public void OnDefiantBuy()
	{
		if (GameInformation.CurrentGame.Diamonds >= currentDefiant.Price) {

			if(Social.Active.localUser.authenticated)
				// Brand new Piece achievement
				GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQAw");

			GameInformation.CurrentGame.GetDefiantByType(currentDefiant.DefiantType).Available = true;
			GameInformation.CurrentGame.GetDefiantByType(currentDefiant.DefiantType).Locked = false;

			bool achieve = true;

			for(int i = 0; i < GameInformation.CurrentGame.DefiantsAvailableStatus.Count; i++)
			{
				if(GameInformation.CurrentGame.DefiantsAvailableStatus[i] != false)
					achieve = false;
			}

			if(achieve && Social.Active.localUser.authenticated)
				// Master of Defiant achievement
				GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQBg");

			itemsPanel[0].GetComponentsInChildren<ItemPanel>()[currentPanel].gameObject.SetActive(false);
			
			for (int i = 0; i < itemsPanel[0].GetComponentsInChildren<Button>().Length; i++) {
				int counter = i;
				itemsPanel[0].GetComponentsInChildren<Button> ()[i].onClick.AddListener (() => OnItemClick(defiants[counter], counter));
			}
			
			defiantNum--;
			if (defiantNum == 0) {
				emptyText.gameObject.SetActive(true);
			}
			SaveLoad.SaveAllInformation();
		}
	}

	public void OnBoostClick(int boost)
	{
		currentBoost = boost;
		string text = "";
		if (currentBoost == 0) {
			text = "Shield";
		} else if (currentBoost == 1) {
			text = "Life Saver";	
		} else {
			text = "Score Boost";
		}
		popup.Pop (text, Boosts.boostPrice);
	}

	public void OnBoostBuy()
	{
		if (GameInformation.CurrentGame.Diamonds >= Boosts.boostPrice) {
			if (currentBoost == 0) {
				GameInformation.CurrentGame.ShieldBoosts += 1;
			} else if (currentBoost == 1) {
				GameInformation.CurrentGame.LifeSavers += 1;	
			} else {
				GameInformation.CurrentGame.ScoreBoosts += 1;
			}
		}
		
	}
}
