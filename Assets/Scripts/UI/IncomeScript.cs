﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IncomeScript : MonoBehaviour {

	public Text coins;
	public Text diamonds;

	void Start () {
		coins.text = GameInformation.CurrentGame.Coins.ToString ();
		diamonds.text = GameInformation.CurrentGame.Diamonds.ToString ();
	}

	void Update()
	{
		coins.text = GameInformation.CurrentGame.Coins.ToString ();
		diamonds.text = GameInformation.CurrentGame.Diamonds.ToString ();
	}
}
