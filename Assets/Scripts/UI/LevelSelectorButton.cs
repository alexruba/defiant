using UnityEngine;
using System.Collections;

public class LevelSelectorButton : MonoBehaviour {

	public void NextLevelButton(int level)
	{
		if(!GameInformation.CurrentGame.isLockedLevel(level))
			Application.LoadLevel("Level" + level);
	}
}
