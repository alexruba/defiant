﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Options : MonoBehaviour {

	public GameObject optionsPanel;
	public Image checkbox;
	public Sprite checkboxOn;
	public Sprite checkboxOff;
	private bool optionsOpened = false;
	public GameObject loadingScreen;
	public Popup popup;

	public void Start()
	{
		if (GameInformation.CurrentGame.accelerometerControl)
			checkbox.overrideSprite = checkboxOn;
		else
			checkbox.overrideSprite = checkboxOff;
	}

	public void CheckboxClick()
	{
		if (GameInformation.CurrentGame.accelerometerControl) {
			GameInformation.CurrentGame.accelerometerControl = false;
			checkbox.overrideSprite = checkboxOff;
		} else {
			GameInformation.CurrentGame.accelerometerControl = true;
			checkbox.overrideSprite = checkboxOn;
		}
	}

	public void OnCloseClick()
	{
		optionsOpened = false;
		optionsPanel.SetActive (false);
		SaveLoad.SaveAllInformation();
	}

	public void OnOpenClick()
	{
		if (optionsOpened) {
			optionsOpened = false;
			optionsPanel.SetActive (false);
		}else {
			optionsOpened = true;
			optionsPanel.SetActive (true);
		}
	}

	public void OnTouchStartClick(string level)
	{
		if (optionsOpened) {
			optionsPanel.SetActive (false);
			optionsOpened = false;
		} else {
			loadingScreen.GetComponent<LoadingScreen> ().OnLevelLoad (level);
		}
	}

	public void OnStorePanelClick(string level)
	{
		if (optionsOpened) {
			optionsPanel.SetActive (false);
			optionsOpened = false;
		} else {
			loadingScreen.GetComponent<LoadingScreen> ().OnLevelLoad (level);
		}
	}

	public void OnRankingPanelClick(string level)
	{
		if (optionsOpened) {
			optionsPanel.SetActive (false);
			optionsOpened = false;
		}else {
			if(FB.IsLoggedIn)
				loadingScreen.GetComponent<LoadingScreen> ().OnLevelLoad (level);
			else
				popup.Pop("You need to login on Facebook first");
		}
	}

	public void OnStoreClick()
	{
		loadingScreen.GetComponent<LoadingScreen> ().OnLevelLoad ("Store");
	}

	public void OnHangarClick()
	{
		loadingScreen.GetComponent<LoadingScreen> ().OnLevelLoad ("Hangar");
	}


}
