using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HangarManager : MonoBehaviour {

	private List<GameObject> defiantsPanel;
	public Transform panelTransform;
	public GameObject panelObject;
	public Button panelButton;
	public GameObject nextButton;
	public GameObject prevButton;
	public Text defiantName;
	public static int currentIndex;
	private bool ready;
	private List<Defiant> defiants;
	public Sprite lockedSprite;
	public Sprite unlockedSprite;
	public Sprite inUseSprite;
	public Text damageText;
	public Text healthText;
	public Text availableText;
	private int selectedDefiantIndex;

	void Start()
	{
		currentIndex = 0;
		ready = true;
		defiantsPanel = new List<GameObject> ();
		defiants = new List<Defiant> ();

		for (int i = 0; i < GameInformation.CurrentGame.Defiants.Count; i++) {
			if (GameInformation.CurrentGame.Defiants [i].Available) {

				if (i == 0)
				{
					panelObject.SetActive (true);
				}
				else
					panelObject.SetActive(false);

				if(!GameInformation.CurrentGame.Defiants[i].Locked)
				{
					if(GameInformation.CurrentGame.CurrentDefiant.DefiantType == GameInformation.CurrentGame.Defiants[i].DefiantType)
					{
						selectedDefiantIndex = defiants.Count;
						panelObject.GetComponent<Image>().sprite = inUseSprite;
						panelButton.image.sprite = GameInformation.CurrentGame.Defiants[i].Mods[GameInformation.CurrentGame.CurrentMods[i]].ModSprite;
					}
					else
					{
						panelObject.GetComponent<Image>().sprite = unlockedSprite;
						panelButton.image.sprite = GameInformation.CurrentGame.Defiants[i].Mods[GameInformation.CurrentGame.CurrentMods[i]].ModSprite;
					}
				}
				else
				{
					panelObject.GetComponent<Image>().sprite = unlockedSprite;
					panelButton.image.sprite = lockedSprite;
				}

				GameObject newPanel = (GameObject)Instantiate (panelObject, panelTransform.position, Quaternion.identity);
				newPanel.name = GameInformation.CurrentGame.Defiants [i].Name;
				newPanel.transform.SetParent (transform);
				newPanel.transform.localScale = new Vector3(1,1,0);

				defiantsPanel.Add (newPanel);
				defiants.Add (GameInformation.CurrentGame.Defiants [i]);
			}
		}
		if(!defiants[0].Locked) {
			defiantsPanel[0].GetComponentInChildren<Button>().onClick.RemoveAllListeners ();
			defiantsPanel[0].GetComponentInChildren<Button>().onClick.AddListener (OnDefiantClick);
		}
		defiantName.text = defiants [0].Name;
		UpdateStats ();
	}

	void UpdateStats()
	{
		if (defiants [currentIndex].Locked) {
			damageText.gameObject.SetActive(false);
			healthText.gameObject.SetActive(false);
			availableText.gameObject.SetActive(true);
		} else {
			damageText.gameObject.SetActive(true);
			healthText.gameObject.SetActive(true);
			damageText.text = "Damage: " + defiants [currentIndex].Stats.damage;
			healthText.text = "Health: " + defiants [currentIndex].Stats.health;
			availableText.gameObject.SetActive(false);
		}

	}

	public void OnDefiantClick()
	{
		defiantsPanel [selectedDefiantIndex].GetComponent<Image> ().sprite = unlockedSprite;
		defiantsPanel [currentIndex].GetComponent<Image> ().sprite = inUseSprite;
		GameInformation.CurrentGame.SetDefiant (defiants [currentIndex].DefiantType);
		SaveLoad.SaveAllInformation();
	}

	public void OnModViewClick()
	{
		GameInformation.CurrentGame.CurrentModView = defiants[currentIndex].DefiantType;
		Application.LoadLevel ("HangarMod");
	}

	void Update()
	{
		if (Input.touchCount > 0 &&
		    Input.GetTouch(0).phase == TouchPhase.Moved && ready) 
		{
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			
			if(touchDeltaPosition.x > 1.5f)
			{
				if(currentIndex > 0)
				{
					ready = false;
					OnPrevClick();
				}
			}
			else if(touchDeltaPosition.x < -1.5f)
			{
				if(currentIndex < defiantsPanel.Count - 1)
				{
					ready = false;
					OnNextClick();
				}
			}
		}

		if (currentIndex == defiantsPanel.Count - 1)
			nextButton.SetActive (false);
		else
			nextButton.SetActive (true);

		if (currentIndex > 0)
			prevButton.SetActive (true);
		else
			prevButton.SetActive (false);
	}

	public void OnNextClick()
	{
		defiantsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("LeaveScreen");
		StartCoroutine (WaitForNextAnimation (0.4f));
	}

	public void OnPrevClick()
	{
		defiantsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Disappear");
		StartCoroutine (WaitForPrevAnimation (0.25f));
	}

	IEnumerator WaitForNextAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		if(currentIndex < defiantsPanel.Count - 1)
			currentIndex ++;

		if(defiantsPanel[currentIndex].activeInHierarchy) defiantsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Next");

		if (defiants [currentIndex].Locked)
			defiantName.text = "??????";
		else
			defiantName.text = defiants [currentIndex].Name;

		UpdateStats ();

		defiantsPanel [currentIndex].SetActive (true);
		if (!defiants [currentIndex].Locked) {
			defiantsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.RemoveAllListeners ();
			defiantsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.AddListener (OnDefiantClick);
		}

		ready = true;
	}

	IEnumerator WaitForPrevAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		
		if (currentIndex > 0)
			currentIndex --;

		if (defiants [currentIndex].Locked)
			defiantName.text = "??????";
		else
			defiantName.text = defiants [currentIndex].Name;

		UpdateStats ();

		defiantsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Prev");
		if (!defiants [currentIndex].Locked) {
			defiantsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.RemoveAllListeners ();
			defiantsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.AddListener (OnDefiantClick);
		}
		ready = true;
	}
}
