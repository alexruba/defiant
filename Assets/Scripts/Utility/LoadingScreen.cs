﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

	public GameObject background;
	public GameObject text;
	public GameObject[] toHide;
	public Slider slider;
	public Text percentage;

	void Start () {
		background.SetActive (false);
		text.SetActive (false);
		slider.gameObject.SetActive (false);
		percentage.gameObject.SetActive (false);
	}

	public void OnLevelLoad (string level) {
		StartCoroutine (DisplayLoadingScreen (level));
	}

	IEnumerator DisplayLoadingScreen(string levelToLoad)
	{
		foreach (GameObject g in toHide)
			g.SetActive (false);
		background.SetActive (true);
		text.SetActive (true);
		percentage.gameObject.SetActive (true);
		percentage.text = "0%";
		slider.value = 0;
		slider.gameObject.SetActive (true);
		AsyncOperation async = Application.LoadLevelAsync (levelToLoad);
		while (!async.isDone) {
			percentage.text = ((int)(async.progress * 100)).ToString() + "%";
			slider.value = (int)(async.progress * 100);
			yield return null;
		}

		yield return null;
	}
}
