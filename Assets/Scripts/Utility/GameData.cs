﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameData : MonoBehaviour {
	
	// Utilities
	public int scoreRate;
	public Text scoreText;
	public static bool gameStarted;
	public Text bestScoreText;
	public int level;

	IEnumerator ScoreIncrementer()
	{
		while(true)
		{
			yield return new WaitForSeconds(1);
			GameInformation.CurrentScore += scoreRate;
			scoreText.text = GameInformation.CurrentScore + "";
		}
	}
	
	void Start()
	{
		GameInformation.CurrentLevel = level;
		GameInformation.CurrentScore = 0;
		bestScoreText.text = "" + GameInformation.CurrentGame.GetLevelScore (GameInformation.CurrentLevel);
	}

	public void StartScoreCount()
	{
		if(gameStarted)
			StartCoroutine(ScoreIncrementer());
	}

	public void StopScoreCount()
	{
		StopCoroutine(ScoreIncrementer());
	}
}
