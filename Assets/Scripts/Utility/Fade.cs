﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fade : MonoBehaviour {
	
	[SerializeField] Text textStart;
	[SerializeField] float fadeDuration;
	
	private void Start ()
	{
		StartCoroutine(StartFading());
	}
	
	private IEnumerator StartFading()
	{
		while (true) 
		{
			yield return StartCoroutine (FadeEffect (0.2f, 1.0f, fadeDuration));
			yield return StartCoroutine (FadeEffect (1.0f, 0.2f, fadeDuration));
			
		}
	}
	
	private IEnumerator FadeEffect (float startLevel, float endLevel, float time)
	{
		float speed = 1.0f/time;
		
		for (float t = 0.0f; t < 1.0; t += Time.deltaTime * speed)
		{
			float a = Mathf.Lerp(startLevel, endLevel, t);
			textStart.color = new Color(textStart.color.r,
			                            textStart.color.g,
			                            textStart.color.b, a);
			yield return 0;
		}
	}
}
