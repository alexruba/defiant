﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup : MonoBehaviour {

	public Text popupText;

	void OnEnable()
	{
		StartCoroutine ("AutoDisable");
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0) && gameObject.activeInHierarchy) {
			gameObject.SetActive(false);
		}
	}

	public void Pop(string text)
	{
		gameObject.SetActive (true);
		popupText.text = text;
	}

	IEnumerator AutoDisable()
	{
		yield return new WaitForSeconds(3);
		gameObject.SetActive (false);
	}
}
