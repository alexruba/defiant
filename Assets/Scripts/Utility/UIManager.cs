﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

	public GameObject pausePanel;
	UnityLevelSelector selector;
	Player player;

	void Start()
	{
		pausePanel.SetActive (false);
		selector = gameObject.GetComponent<UnityLevelSelector> ();
	}

	void Update()
	{
		if (Input.touchCount >= 2) {
			pausePanel.SetActive(true);
			Time.timeScale = 0;
		}
	}

	public void Pause()
	{
		pausePanel.SetActive (true);
		Time.timeScale = 0;
	}

	public void Retry()
	{
		selector.NextLevelButton ("Level" + GameInformation.CurrentLevel);
		GameInformation.CurrentScore = 0;
		Player.superAttackCounter = 0;
		Time.timeScale = 1;
	}

	public void Menu()
	{
		selector.NextLevelButton ("Menu");
		Player.superAttackCounter = 0;
		Time.timeScale = 1;
	}

	public void Resume()
	{
		pausePanel.SetActive (false);
		Time.timeScale = 1;
	}
}
