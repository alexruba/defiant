﻿//This is a modification of the original code found here: http://forum.unity3d.com/threads/simple-reusable-object-pool-help-limit-your-instantiations.76851/
//Remember kids, credit where credit is due!
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolScript : MonoBehaviour
{
	public static ObjectPoolScript current;			//A public static reference to itself (make's it visible to other objects without a reference)
	public List<GameObject> prefabs;				//Collection of prefabs to be poooled
	public List<GameObject>[] pooledObjects;	//The actual collection of pooled objects
	public int[] amountToBuffer;				//The amount to pool of each object. This is optional
	public int defaultBufferAmount = 10;		//Default pooled amount if no amount abaove is supplied
	public bool canGrow = true;					//Whether or not the pool can grow. Should be off for final builds
	
	GameObject containerObject;					//A parent object for pooled objects to be nested under. Keeps the hierarchy clean
	
	
	void Awake ()
	{
		//Ensure that there is only one object pool
		if (current == null)
			current = this;
		else
			Destroy(gameObject);

		// Search for current defiant prefab and retrieve its bullet and add it to prefabs
		GameInformation.CurrentGame.CurrentDefiantModType = 
			GameInformation.CurrentGame.Defiants [GameInformation.CurrentGame.CurrentDefiantIndex].Mods [GameInformation.CurrentGame.CurrentMods [GameInformation.CurrentGame.CurrentDefiantIndex]].ModType;
		GameObject defiantPrefab = GameInformation.CurrentGame.Defiants [GameInformation.CurrentGame.CurrentDefiantIndex].Mods [GameInformation.CurrentGame.CurrentMods [GameInformation.CurrentGame.CurrentDefiantIndex]].ModPrefab;
		GameObject bullet = defiantPrefab.GetComponent<Player>().Bullet;
		prefabs.Add (bullet);
		GameObject bulletEffect = bullet.transform.GetChild (0).Find ("Bullet").GetComponent<BulletDamageScript> ().collisionEffect;
		prefabs.Add(bulletEffect);
	
		// If it uses BulletPatternAttack, add that bullet too
		if (defiantPrefab.GetComponent<BulletPatternAttack> () != null) {
			GameObject superBullet = defiantPrefab.GetComponent<BulletPatternAttack> ().bullet;
			prefabs.Add (superBullet);
			GameObject superEffect = superBullet.transform.GetChild(0).Find("Bullet").GetComponent<BulletDamageScript>().collisionEffect;
			if(!superEffect.name.Equals(bulletEffect.name))
				prefabs.Add (superBullet.transform.GetChild(0).Find("Bullet").GetComponent<BulletDamageScript>().collisionEffect);
		}

		//Create new container
		containerObject = new GameObject("ObjectPoolContainer");

		//Create new list for objects
		pooledObjects = new List<GameObject>[prefabs.Count];
		
		int index = 0;
		//For each prefab to be pooled...
		foreach ( GameObject objectPrefab in prefabs )
		{
			//...create a new array for the objects then...
			pooledObjects[index] = new List<GameObject>(); 
			//...determine the amount to be created then...
			int bufferAmount;
			if(index < amountToBuffer.Length) 
				bufferAmount = amountToBuffer[index];
			else
				bufferAmount = defaultBufferAmount;
			//...loop the correct number of times and in each iteration...
			for ( int i = 0; i < bufferAmount; i++)
			{
				//...create the object...
				GameObject obj = (GameObject)Instantiate(objectPrefab);
				//...give it a name...
				obj.name = objectPrefab.name;
				//...and add it to the pool.
				PoolObject(obj);
			}
			//Go to the next prefab in the collection
			index++;
		}


	}
	
	public GameObject GetObject( GameObject objectType)
	{
		//Loop through the collection of prefabs...
		for(int i=0; i<prefabs.Count; i++)
		{
			//...to find the one of the correct type
			GameObject prefab = prefabs[i];
			if(prefab.name == objectType.name)
			{
				for(int j = 0; j < pooledObjects[i].Count; j++)
				{
					if(!pooledObjects[i][j].activeInHierarchy)
					{
						return pooledObjects[i][j];
					}
				}

				//Otherwise, if the pool is allowed to grow...
				if(canGrow) 
				{
					//...create a new one...
					GameObject obj = Instantiate(prefabs[i]) as GameObject;
					//...give it a name...
					obj.name = prefabs[i].name;
					//...and return it.
					return obj;
				}
				//If we found the correct collection but it is empty and can't grow, break out of the loop
				break;
				
			}
		}
		
		return null;
	}
	
	public void PoolObject ( GameObject obj )
	{
		//Find the correct pool for the object to go in to
		for ( int i=0; i<prefabs.Count; i++)
		{
			if(prefabs[i].name == obj.name)
			{
				//Deactivate it...
				obj.SetActive(false);
				//..parent it to the container...
				obj.transform.SetParent(containerObject.transform);
				//...and add it back to the pool
				pooledObjects[i].Add(obj);
				return;
			}
		}
	}
	
}