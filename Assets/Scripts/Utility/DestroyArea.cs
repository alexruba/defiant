﻿using UnityEngine;
using System.Collections;

public class DestroyArea : MonoBehaviour {

	private Player player;
	public float damage;
	public float penetration;

	void Start()
	{
		player = GameObject.FindWithTag ("Player").GetComponent<Player> ();
		float cameraHeight = 2f * Camera.main.orthographicSize;
		float cameraWidth = cameraHeight * Camera.main.aspect;
		gameObject.GetComponent<EdgeCollider2D> ().Reset ();
		Vector2 pos = new Vector2 (-cameraWidth, -cameraHeight / 2 - 2f);
		Vector2 pos2 = new Vector2 (cameraWidth, -cameraHeight / 2 - 2f);
		Vector2[] positions = new Vector2[2];
		positions [0] = pos;
		positions [1] = pos2;
		gameObject.GetComponent<EdgeCollider2D> ().points = positions;
		gameObject.GetComponent<EdgeCollider2D> ().isTrigger = true;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag != "Player") {
			if(other.gameObject.tag.Equals("Enemy"))
			{
				player.TakeDamage(damage, penetration);
				Destroy (other.gameObject);
			}
			else
				other.gameObject.SetActive (false);
		}

	}
}
