﻿using UnityEngine;
using System.Collections;

public class DestroyScript : MonoBehaviour {

	public float destroyInTime;
	
	void OnEnable()
	{
		Invoke ("Destroy", destroyInTime);
	}
	
	void Destroy()
	{
		gameObject.SetActive (false);
	}
	
	void OnDisable()
	{
		CancelInvoke ();
	}
}
