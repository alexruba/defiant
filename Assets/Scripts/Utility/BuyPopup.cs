﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyPopup : MonoBehaviour {

	public Text popupText;
	public Text priceText;
	public enum PaymentMethod { COIN, DIAMOND};
	public PaymentMethod method;
	public Sprite coinSprite;
	public Sprite diamondSprite;
	public Image methodImage;
	public Popup popup;

	void Start()
	{
		if (method == PaymentMethod.COIN)
			methodImage.sprite = coinSprite;
		else
			methodImage.sprite = diamondSprite;
	}
	
	public void Pop(string text, int price)
	{
		gameObject.SetActive (true);
		popupText.text = text;
		priceText.text = price.ToString();
	}

	public void OnCancel()
	{
		gameObject.SetActive (false);
	}

	public void OnBuy()
	{
		if (method == PaymentMethod.COIN) {
			if(GameInformation.CurrentGame.Coins >= int.Parse(priceText.text))
			{
				GameObject.Find ("MoneyClip").GetComponent<AudioSource>().Play();
				popup.Pop("Purchase completed");
				GameInformation.CurrentGame.Coins -= int.Parse (priceText.text);
			}
			else
			{
				GameObject.Find ("NoMoneyClip").GetComponent<AudioSource>().Play();
				popup.Pop("Insufficient funds");
			}
		} else if (method == PaymentMethod.DIAMOND) {
			if(GameInformation.CurrentGame.Diamonds >= int.Parse(priceText.text))
			{
				GameObject.Find ("MoneyClip").GetComponent<AudioSource>().Play();
				popup.Pop("Purchase completed");
				GameInformation.CurrentGame.Diamonds -= int.Parse(priceText.text);
			}
			else
			{
				GameObject.Find ("NoMoneyClip").GetComponent<AudioSource>().Play();
				popup.Pop("Insufficient funds");
			}
		}

		gameObject.SetActive (false);
	}
}
