﻿using UnityEngine;
using System.Collections;

public class Skin {

	public enum Type{ RED_DEFAULT, RED_MASTER, GREEN_DEFAULT, BLUE_DEFAULT};
	public Type skinType;
	public Sprite skinSprite;
	public float price;
	private bool locked;
	
	public Skin(Type skinType, Sprite skinSprite, float price, bool locked)
	{
		this.skinType = skinType;
		this.skinSprite = skinSprite;
		this.price = price;
		this.locked = locked;
	}

	public Type SkinType {
		get{ return skinType;}
		set{ skinType = value;}
	}

	public Sprite SkinSprite {
		get{ return skinSprite;}
		set{ skinSprite = value;}
	}
	public bool Locked {
		get{ return locked;}
		set{ locked = value;}
	}
	public float Price {
		get{ return price;}
		set{ price = value;}
	}
}
