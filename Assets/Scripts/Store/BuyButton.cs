﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyButton : MonoBehaviour {

	public Image buttonImage;
	public Text buttonText;
	public Sprite coinSprite;
	public Sprite diamondSprite;

	public void SetButton(Drop.Type type, string text)
	{
		if (type == Drop.Type.COIN)
			buttonImage.sprite = coinSprite;
		else
			buttonImage.sprite = diamondSprite;

		buttonText.text = text;
	}
}
