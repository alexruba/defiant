﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemPanel : MonoBehaviour {

	public BuyButton buyButton;
	public Image itemImage;
	public Text itemName;
	private int index;

	public void SetImage(Sprite sprite)
	{
		itemImage.sprite = sprite;
	}

	public void SetText(string text)
	{
		itemName.text = text;
	}

	public void SetButton(float price, int type, int index)
	{
		buyButton.SetButton (Drop.Type.DIAMOND, price.ToString ());
	}

}
