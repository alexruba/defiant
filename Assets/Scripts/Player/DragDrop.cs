﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]

public class DragDrop : MonoBehaviour {

	public bool dragging;

	void OnMouseDrag()
	{
		dragging = true;
		Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
		transform.position = new Vector2(curPosition.x, curPosition.y + 0.8f);
	}

	void OnMouseUp()
	{
		dragging = false;
	}
	
}