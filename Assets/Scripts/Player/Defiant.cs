﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Defiant {

	public enum Type{ CLASSIC, ALPHA, OMIUM}
	public enum SuperType{ LASER, STATS, BULLET, ALLY}
	private GameObject defiantPrefab;
	private bool locked;
	private bool available;
	private float price;
	private Type defiantType;
	private Sprite defiantSprite;
	private float damage;
	private float penetration;
	private float health;
	private SuperType superAttack;
	private Player stats;
	private string name;
	private List<DefiantMod> mods;
	private DefiantMod.Type currentModType;
	
	public Defiant(GameObject defiantPrefab, bool locked, bool available, float price, Type defiantType, Sprite defiantSprite, SuperType superAttack, List<DefiantMod> mods, string name)
	{
		this.defiantPrefab = defiantPrefab;
		this.locked = locked;
		this.available = available;
		this.price = price;
		this.defiantType = defiantType;
		this.defiantSprite = defiantSprite;
		this.superAttack = superAttack;
		this.stats = defiantPrefab.GetComponent<Player> ();
		this.mods = mods;
		this.name = name;
	}

	public DefiantMod GetModWithType(DefiantMod.Type type)
	{
		for (int i = 0; i < mods.Count; i++) {
			if(mods[i].ModType == type)
				return mods[i];
		}
		return mods [0];
	}

	public GameObject DefiantPrefab {
		get{ return defiantPrefab; }
		set{ defiantPrefab = value; }
	}
	public bool Locked {
		get{ return locked;}
		set{ locked = value;}
	}
	public bool Available {
		get{ return available;}
		set{ available = value;}
	}
	public float Price {
		get{ return price;}
		set{ price = value;}
	}
	public Type DefiantType {
		get{ return defiantType;}
		set{ defiantType = value;}
	}
	public Sprite DefiantSprite {
		get{ return defiantSprite;}
		set{ defiantSprite = value;}
	}
	public SuperType SuperAttack {
		get{ return superAttack;}
		set{ superAttack = value;}
	}
	public Player Stats {
		get{ return stats;}
		set{ stats = value;}
	}
	public DefiantMod.Type CurrentModType {
		get{ return currentModType; }
		set{ currentModType = value; }
	}
	public List<DefiantMod> Mods {
		get{ return mods; }
		set{ mods = value; }
	}
	public string Name {
		get{ return name;}
		set{ name = value;}
	}

}
