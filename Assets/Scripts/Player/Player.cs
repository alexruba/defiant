using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : Spaceship {

	// Movement parameters
	private float smooth = 0.5f;
	private Vector3 zeroAc = new Vector3();
	private Vector3 curAc;
	private float sensH = 3;
	private float sensV = 3;

	private GameObject healthBar;
	private GameObject superBar;
	public GameObject bullet;
	private GameObject scoretext;
	private GameObject scorePanel;
	private GameObject laserEffectUI;
	private GameObject greyPanel;
	private GameObject bestScoreText;

	private Slider healthSlider;
	private Slider superSlider;
	public static float superAttackCounter = 0;
	private float lastUpdate;
	private BoxCollider2D boxCollider;
	private CircleCollider2D circleCollider;
	private GameObject damageSplash;
	public float superAttackBoost;
	public bool lifeSaver;
	public float currentHealth;
	private DragDrop dragDrop;

	void Start() 
	{
		currentHealth = health;
		healthBar = GameObject.Find ("HealthBar");
		superBar = GameObject.Find ("SuperBar");
		laserEffectUI = GameObject.Find ("LaserEffect");
		laserEffectUI.SetActive (false);
		scoretext = GameObject.Find ("Score");
		scorePanel = GameObject.Find ("ScoreResult");
		bestScoreText = scorePanel.transform.Find ("BestScore").gameObject;
		bestScoreText.SetActive (false);
		scorePanel.SetActive (false);
		damageSplash = GameObject.Find ("DamageSplash");
		damageSplash.SetActive (false);
		greyPanel = GameObject.Find ("GreyPanel");
		greyPanel.SetActive (false);
		healthSlider = healthBar.GetComponentInChildren<Slider> ();
		healthSlider.minValue = 0;
		healthSlider.maxValue = GameInformation.CurrentGame.CurrentDefiant.Stats.health;
		superSlider = superBar.GetComponentInChildren<Slider> ();
		healthSlider.value = currentHealth;
		superSlider.value = superAttackCounter;
		boxCollider = gameObject.GetComponent<BoxCollider2D> ();
		circleCollider = gameObject.GetComponent<CircleCollider2D> ();
		dragDrop = gameObject.GetComponent<DragDrop> ();

		if (shield <= 0) {
			boxCollider.enabled = true;
			circleCollider.enabled = false;
		} else {
			boxCollider.enabled = false;
			circleCollider.enabled = true;
		}

		StartCoroutine ("Fire");
	}

	void Update()
	{
		superSlider.value = superAttackCounter;
		if (superAttackCounter >= 100)
			TrySuperAttack ();
	}

	public void TrySuperAttack()
	{
		if (superAttackCounter >= 100) {
			superAttackCounter = 0;
			superBar.GetComponentInChildren<Slider> ().value = 0;

			if(GameInformation.CurrentGame.CurrentDefiant.SuperAttack == Defiant.SuperType.LASER)
			{
				gameObject.GetComponent<LaserAttack>().laserEffect = laserEffectUI;
				gameObject.GetComponent<LaserAttack>().Attack();
			}
			else if(GameInformation.CurrentGame.CurrentDefiant.SuperAttack == Defiant.SuperType.STATS)
				gameObject.GetComponent<StatsAttack>().Attack();
			else if(GameInformation.CurrentGame.CurrentDefiant.SuperAttack == Defiant.SuperType.BULLET)
				gameObject.GetComponent<BulletPatternAttack>().Attack();
			else if(GameInformation.CurrentGame.CurrentDefiant.SuperAttack == Defiant.SuperType.ALLY)
				gameObject.GetComponent<AllyAttack>().Attack();
		}
	}

	IEnumerator Fire()
	{
		while (true) {
			if(dragDrop.dragging)
			{
				GameObject obj = ObjectPoolScript.current.GetObject (bullet);
				if (obj == null) yield break;
				
				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
				
				ActivateObjectAndChildren(obj);
				
				yield return new WaitForSeconds (shootingRate);
			}	
			else
				yield return null;
		}
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals ("Enemy")) {
			TakeDamage(other.gameObject.GetComponent<Enemy>().damage, 0.5f);
			Destroy(other.gameObject);
		}
	}

	public void IncreaseHealth(float healthBoost)
	{
		if (this.currentHealth + healthBoost < 100)
			this.currentHealth += healthBoost;
		else
			this.currentHealth = health;

		healthSlider.value = this.currentHealth;
	}

	public void TakeDamage(float damage, float penetration)
	{
		GameObject.Find ("Damaged").GetComponent<AudioSource>().Play();

		if (damage > 0)
			GameInformation.Combo = 0;

		if (shield <= 0) {
			currentHealth -= damage;
			boxCollider.enabled = true;
			circleCollider.enabled = false;
		}
		else {
			currentHealth -= penetration * damage / 2;
			shield -= penetration * damage;
			boxCollider.enabled = false;
			circleCollider.enabled = true;
		}

		healthSlider.value = currentHealth;
		StartCoroutine ("DamageFlash");
		if (currentHealth <= 0) {

			GameObject obj = ObjectPoolScript.current.GetObject (explosion);
			if (obj != null) {
				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
			}
			obj.SetActive (true);

			if(GameInformation.CurrentGame.SetScoreLevel (GameInformation.CurrentLevel, GameInformation.CurrentScore))
			{
				GameInformation.CurrentGame.SetNewScore();
				bestScoreText.SetActive(true);
			}

			scoretext.GetComponent<Text> ().text = "" + GameInformation.CurrentScore;
			scorePanel.SetActive (true);
			GameInformation.Instance.UpdateAchievements();
			MusicSingleton.instance.audioSource.Stop();
			GameObject.Find ("GameOver").GetComponent<AudioSource>().Play();
			greyPanel.SetActive (true);
			GameObject.Find("WaveManager").SetActive(false);
			GameObject.Find ("AsteroidManager").SetActive(false);
			SaveLoad.SaveAllInformation ();

			gameObject.SetActive (false);
			Time.timeScale = 0;

		} else if (currentHealth <= 20 && lifeSaver) {
			currentHealth = health;
			lifeSaver = false;
		}
	}

	IEnumerator DamageFlash()
	{
		damageSplash.SetActive (true);
		yield return new WaitForSeconds (0.3f);
		damageSplash.SetActive (false);
	}


	void FixedUpdate()
	{
		if (GameInformation.CurrentGame.accelerometerControl) {
			float x = 0f;
			float y = 0f;
		
			curAc = Vector3.Lerp(curAc, Input.acceleration-zeroAc, Time.deltaTime/smooth);
			x = Mathf.Clamp(curAc.x * sensV, -1, 1);
			y = Mathf.Clamp(curAc.y * sensH, -1, 1);
			
			Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0,0));
			Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1,1));
			
			Vector2 pos = transform.position;
			
			pos += new Vector2(x, y) * speed * Time.deltaTime;
			
			pos.x = Mathf.Clamp (pos.x, min.x, max.x);
			pos.y = Mathf.Clamp (pos.y, min.y, max.y);
			
			transform.position = pos;
		} 

	}

	public GameObject Bullet {
		get{ return bullet;}
		set{ bullet = value;}
	}
}
