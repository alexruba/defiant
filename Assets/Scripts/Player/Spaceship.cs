﻿using UnityEngine;
using System.Collections;

public class Spaceship : MonoBehaviour {

	public float speed;
	public float damage;
	public float health;
	public float penetration;
	public float shield;
	public float shootingRate;
	public GameObject explosion;

	public enum SPACESHIP_TYPE{
		PLAYER,
		RANGER,
		BLASTER,
		LEOPARD,
		BOMBER
	};

	public void DestroySpaceship()
	{
		GameObject obj = ObjectPoolScript.current.GetObject (explosion);
		if (obj == null) return;
		
		obj.transform.position = transform.position;
		obj.transform.rotation = transform.rotation;

		obj.SetActive (true);

		Destroy (gameObject);
	}

	public void ActivateObjectAndChildren(GameObject obj)
	{
		for (int i = 0; i < obj.transform.childCount; ++i)
		{
			obj.transform.GetChild(i).gameObject.SetActive(true);
		}
		
		obj.SetActive(true);
	}
}
