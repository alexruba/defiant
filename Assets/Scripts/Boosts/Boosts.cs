﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Boosts : MonoBehaviour {

	public Image shield;
	public Image health;
	public Image score;
	private Color originalColor;
	private Color pressedColor;
	public Color noneColor;
	public Color haveColor;
	public Text shields;
	public Text lifeSavers;
	public Text scoreBoost;
	public BuyPopup popup;
	private string currentBoost;
	private bool shieldPressed;
	private bool healthPressed;
	private bool scorePressed;
	public static int boostPrice = 2;
	public enum Type{ LIFESAVER, SCOREBOOST, SHIELD};

	void Start()
	{
		shields.text = GameInformation.CurrentGame.ShieldBoosts.ToString ();
		lifeSavers.text = GameInformation.CurrentGame.LifeSavers.ToString ();
		scoreBoost.text = GameInformation.CurrentGame.ScoreBoosts.ToString ();

		originalColor = shield.color;
		pressedColor = new Color (originalColor.r, originalColor.g, originalColor.b, 255);

		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().shield = 0;
		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().lifeSaver = false;
		GameObject.Find ("GameData").GetComponent<GameData> ().scoreRate = 100;

		shieldPressed = false;
		healthPressed = false;
		scorePressed = false;

		UpdateNumbersColor();
	}

	public void OnBoostClick(string currentBoost)
	{
		this.currentBoost = currentBoost;
		if (currentBoost.Equals ("shield")) {
			if (!shieldPressed) {
				if (int.Parse (shields.text) == 0)
					popup.Pop ("Shield Boost", boostPrice);
				else
					GetShield ();
			} else
				OnShield ();
		} else if (currentBoost.Equals ("health")) {
			if (!healthPressed) {
				if (int.Parse (lifeSavers.text) == 0)
					popup.Pop ("Life Saver", boostPrice);
				else 
					GetLifeSaver ();
			} else
				OnHealth ();
		} else if(currentBoost.Equals("score")) {
			if(!scorePressed) {
				if (int.Parse (scoreBoost.text) == 0)
					popup.Pop ("Score Boost", boostPrice);
				else
					GetScoreBoost();
			}else
				OnScore();
		}

	}

	public void UpdateNumbersColor()
	{
		if (GameInformation.CurrentGame.ShieldBoosts == 0) 
			shields.color = noneColor;
		else
			shields.color = haveColor;
		
		if (GameInformation.CurrentGame.LifeSavers == 0) 
			lifeSavers.color = noneColor;
		else
			lifeSavers.color = haveColor;
		
		if (GameInformation.CurrentGame.ScoreBoosts == 0) 
			scoreBoost.color = noneColor;
		else
			scoreBoost.color = haveColor;
	}

	public void GetShield()
	{
		shieldPressed = true;
		shield.color = pressedColor;
		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().shield = 100;
		GameInformation.CurrentGame.ShieldBoosts -= 1;
		shields.text = GameInformation.CurrentGame.ShieldBoosts.ToString ();
		UpdateNumbersColor ();
	}

	public void GetLifeSaver()
	{
		healthPressed = true;
		health.color = pressedColor;
		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().lifeSaver = true;
		GameInformation.CurrentGame.LifeSavers -= 1;
		lifeSavers.text = GameInformation.CurrentGame.LifeSavers.ToString ();
		UpdateNumbersColor ();
	}

	public void GetScoreBoost()
	{
		scorePressed = true;
		score.color = pressedColor;
		GameObject.Find ("GameData").GetComponent<GameData> ().scoreRate = 125;
		GameInformation.CurrentGame.ScoreBoosts -= 1;
		scoreBoost.text = GameInformation.CurrentGame.ScoreBoosts.ToString ();
		UpdateNumbersColor ();
	}
	
	public void OnHealth()
	{
		healthPressed = false;
		health.color = originalColor;
		GameInformation.CurrentGame.LifeSavers += 1;
		lifeSavers.text = GameInformation.CurrentGame.LifeSavers.ToString ();
		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().lifeSaver = false;
		UpdateNumbersColor ();
	}
	public void OnScore()
	{
		scorePressed = false;
		score.color = originalColor;
		GameInformation.CurrentGame.ScoreBoosts += 1;
		scoreBoost.text = GameInformation.CurrentGame.ScoreBoosts.ToString ();
		GameObject.Find ("GameData").GetComponent<GameData> ().scoreRate = 100;
		UpdateNumbersColor ();
	}

	public void OnShield()
	{
		shieldPressed = false;
		shield.color = originalColor;
		GameInformation.CurrentGame.ShieldBoosts += 1;
		shields.text = GameInformation.CurrentGame.ShieldBoosts.ToString ();
		GameInformation.CurrentGame.CurrentDefiant.DefiantPrefab.GetComponent<Player> ().shield = 0;
		UpdateNumbersColor ();
	}

	public void OnBoostBought()
	{
		if (GameInformation.CurrentGame.Diamonds >= boostPrice) {

			if(Social.Active.localUser.authenticated)
				// Boosts are the future achievement
				GooglePlayServices.UnlockAchievement("CgkIxfvWvYwGEAIQHA");

			switch (currentBoost) {
			case "shield":
				OnShield ();
				break;
			case "score":
				OnScore ();
				break;
			case "health":
				OnHealth ();
				break;
			}
		}
	}
}
