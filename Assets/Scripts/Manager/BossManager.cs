﻿using UnityEngine;
using System.Collections;

public class BossManager : MonoBehaviour {

	GameObject waveManager;
	GameObject asteroidManager;
	public GameObject[] bosses;
	private bool bossSpanwed;

	void Start()
	{
		waveManager = GameObject.Find ("WaveManager");
		asteroidManager = GameObject.Find ("AsteroidManager");
	}

	// Update is called once per frame
	void Update () {
		if(!bossSpanwed && GameInformation.CurrentScore >= 1000)
		{
			bossSpanwed = true;
			waveManager.SetActive(false);
			asteroidManager.SetActive(false);

			Instantiate(bosses[0], transform.position, Quaternion.identity);
		}
	}
}
