﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AsteroidManager : MonoBehaviour {

	public GameObject[] asteroids;
	public float rateTime;
	public float startTime;
	public float stopTime;
	private float currentTime;
	public GameObject meteorRainImage;
	public GameObject cautionSign;

	public void Start()
	{
		StartCoroutine("InvokeAsteroid");
	}
	
	IEnumerator InvokeAsteroid() {
		while (true) {
			currentTime = 0;
			yield return new WaitForSeconds(startTime);
			meteorRainImage.SetActive(true);
			while (currentTime < stopTime) {
				currentTime += Time.deltaTime * 100;
				int index = Random.Range (0, asteroids.Length);
				int numberOfAsteroids = Random.Range(1, 4);
				for(int i = 0; i < numberOfAsteroids; i++)
				{
					GameObject obj = ObjectPoolScript.current.GetObject (asteroids[index]);
					if (obj == null) yield break;
					
					float cameraHeight = 2f * Camera.main.orthographicSize;
					float cameraWidth = cameraHeight * Camera.main.aspect;
					
					float x = Random.Range(-cameraWidth / 2 + 0.1f * cameraWidth, cameraWidth / 2 - 0.1f * cameraWidth);
					Destroy (Instantiate (cautionSign, new Vector2(x, cameraHeight/2) + new Vector2(0, -1f), Quaternion.identity), 0.85f);
					yield return new WaitForSeconds(0.40f);
					obj.transform.position = new Vector2(x, transform.position.y);
					obj.transform.rotation = transform.rotation;
					obj.SetActive (true);
				}
				float rate = Random.Range(2.5f, rateTime);
				yield return new WaitForSeconds(rate);
			}
			meteorRainImage.SetActive (false);
		}

	}
}
