﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserManager : MonoBehaviour {

	public GameObject laser;
	public float startTime;
	public float minTime;
	public float maxtime;

	private float cameraHeight;
	private float cameraWidth;

	private List<Vector2> laserPositions;
	private Vector2 laserPosition;

	public GameObject cautionSign;

	void Start()
	{
		cameraHeight = 2f * Camera.main.orthographicSize;
		cameraWidth = cameraHeight * Camera.main.aspect;
		laserPositions = new List<Vector2> ();
		StartCoroutine ("LaserSpawn");
	}
	
	IEnumerator LaserSpawn()
	{
		yield return new WaitForSeconds (startTime);
		
		while(true)
		{
			laserPositions.Clear();

			int numberOfSimultaneousLasers = Random.Range(1, 3);

			for(int i = 0; i < numberOfSimultaneousLasers; i++)
			{
				laserPosition = new Vector2(-cameraWidth/2, Random.Range(cameraHeight /3, -cameraHeight/3));
				laserPositions.Add(laserPosition);
				Destroy (Instantiate (cautionSign, laserPosition + new Vector2(0.5f, 0), Quaternion.identity),0.5f);
				yield return new WaitForSeconds (0.5f);
				Instantiate(laser, laserPosition, laser.transform.rotation);
			}

			yield return new WaitForSeconds(Random.Range(minTime, maxtime));
		}
	}
}
