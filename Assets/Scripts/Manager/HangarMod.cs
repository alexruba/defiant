﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HangarMod : MonoBehaviour {

	public Text modName;
	public Text defiantName;
	private Defiant current;
	private int currentIndex;
	private bool ready;
	private List<GameObject> modsPanel;
	private List<DefiantMod> defiantMods;
	public GameObject panelObject;
	public Transform panelTransform;
	public Sprite inUseSprite;
	public Sprite lockedSprite;
	public Sprite unlockedSprite;
	public Button panelButton;
	public GameObject nextButton;
	public GameObject prevButton;
	public GameObject buyButton;
	public Text priceText;
	private int selectedModIndex;
	public Popup popup;

	void Start()
	{
		current = GameInformation.CurrentGame.GetDefiantByType (GameInformation.CurrentGame.CurrentModView);
		defiantName.text = current.DefiantType.ToString();

		currentIndex = 0;
		ready = true;
		modsPanel = new List<GameObject> ();
		defiantMods = new List<DefiantMod> ();
		
		for (int i = 0; i < current.Mods.Count; i++) {
				
			if (i == 0)
			{
				panelObject.SetActive (true);
			}
			else
				panelObject.SetActive(false);
			
			if(!current.Mods[i].Locked)
			{
				if(GameInformation.CurrentGame.CurrentMods[(int)current.DefiantType] == (int)current.Mods[i].ModType)
				{
					selectedModIndex = defiantMods.Count;
					panelObject.GetComponent<Image>().sprite = inUseSprite;
					panelButton.image.sprite = current.Mods[i].ModSprite;
				}
				else
				{
					panelObject.GetComponent<Image>().sprite = unlockedSprite;
					panelButton.image.sprite = current.Mods[i].ModSprite;
				}
			}
			
			else
			{
				panelObject.GetComponent<Image>().sprite = unlockedSprite;
				panelButton.image.sprite = lockedSprite;
			}
			
			GameObject newPanel = (GameObject)Instantiate (panelObject, panelTransform.position, Quaternion.identity);
			newPanel.name = current.Mods[i].ModType.ToString();
			newPanel.transform.SetParent (transform);
			newPanel.transform.localScale = new Vector3(1,1,0);
			
			modsPanel.Add (newPanel);
			defiantMods.Add (current.Mods[i]);

		}
		if(!defiantMods[0].Locked) {
			modsPanel[0].GetComponentInChildren<Button>().onClick.RemoveAllListeners ();
			modsPanel[0].GetComponentInChildren<Button>().onClick.AddListener (OnModClick);
		}
		modName.text = defiantMods [currentIndex].ModType.ToString();
	}

	public void OnBuyClick()
	{
		if (GameInformation.CurrentGame.Coins >= int.Parse (priceText.text)) {
			GameInformation.CurrentGame.Coins -= int.Parse (priceText.text);

			GameObject.Find ("MoneyClip").GetComponent<AudioSource>().Play();

			current.Mods [currentIndex].Locked = false;
			modsPanel [currentIndex].GetComponentInChildren<Button> ().image.sprite = defiantMods [currentIndex].ModSprite;
			modName.text = defiantMods[currentIndex].ModType.ToString();
			buyButton.SetActive (false);
			popup.Pop( defiantMods[currentIndex].ModType.ToString() + " acquired");

			if (!defiantMods [currentIndex].Locked) {
				modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.RemoveAllListeners ();
				modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.AddListener (OnModClick);
			}

			SaveLoad.SaveAllInformation();
		} else {
			GameObject.Find ("NoMoneyClip").GetComponent<AudioSource>().Play();
			popup.Pop("Insufficient coins");
		}
	}

	public void OnModClick()
	{
		modsPanel [selectedModIndex].GetComponent<Image> ().sprite = unlockedSprite;
		modsPanel [currentIndex].GetComponent<Image> ().sprite = inUseSprite;
		GameInformation.CurrentGame.SetDefiantMod (current, defiantMods [currentIndex].ModType);
		SaveLoad.SaveAllInformation();
	}

	void Update()
	{
		if (Input.touchCount > 0 &&
		    Input.GetTouch(0).phase == TouchPhase.Moved && ready) 
		{
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			
			if(touchDeltaPosition.x > 1.5f)
			{
				if(currentIndex > 0)
				{
					ready = false;
					OnPrevClick();
				}
			}
			else if(touchDeltaPosition.x < -1.5f)
			{
				if(currentIndex < modsPanel.Count - 1)
				{
					ready = false;
					OnNextClick();
				}
			}
		}
		
		if (currentIndex == modsPanel.Count - 1)
			nextButton.SetActive (false);
		else
			nextButton.SetActive (true);
		
		if (currentIndex > 0)
			prevButton.SetActive (true);
		else
			prevButton.SetActive (false);
	}
	
	public void OnNextClick()
	{
		modsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("LeaveScreen");
		StartCoroutine (WaitForNextAnimation (0.4f));
	}
	
	public void OnPrevClick()
	{
		modsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Disappear");
		StartCoroutine (WaitForPrevAnimation (0.25f));
	}
	
	IEnumerator WaitForNextAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		if(currentIndex < modsPanel.Count - 1)
			currentIndex ++;
		
		if(modsPanel[currentIndex].activeInHierarchy) modsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Next");
		
		if (defiantMods [currentIndex].Locked) {
			modName.text = "";
			priceText.text = defiantMods [currentIndex].Price.ToString();
			buyButton.SetActive (true);
		} else {
			buyButton.SetActive(false);
			modName.text = defiantMods [currentIndex].ModType.ToString ();
		}

		modsPanel [currentIndex].SetActive (true);
		if (!defiantMods [currentIndex].Locked) {
			modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.RemoveAllListeners ();
			modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.AddListener (OnModClick);
		}
		
		ready = true;
	}
	
	IEnumerator WaitForPrevAnimation(float seconds)
	{
		yield return new WaitForSeconds (seconds);
		
		if (currentIndex > 0)
			currentIndex --;
		
		if (defiantMods [currentIndex].Locked) {
			modName.text = "";
			priceText.text = defiantMods [currentIndex].Price.ToString();
			buyButton.SetActive (true);
		} else {
			buyButton.SetActive(false);
			modName.text = defiantMods [currentIndex].ModType.ToString ();
		}

		modsPanel [currentIndex].GetComponent<Animator> ().SetTrigger ("Prev");
		if (!defiantMods [currentIndex].Locked) {
			modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.RemoveAllListeners ();
			modsPanel [currentIndex].GetComponentInChildren<Button> ().onClick.AddListener (OnModClick);
		}
		ready = true;
	}
}
