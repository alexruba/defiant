﻿using UnityEngine;
using System.Collections;

public class DefiantMod {

	public enum Type { DEFAULT, POWERED, MASTER, GOLD }
	private GameObject modPrefab;
	private Sprite modSprite;
	private Type modType;
	private float price;
	private bool locked;

	public DefiantMod(GameObject modPrefab, Sprite modSprite, float price, bool locked, Type modType)
	{
		this.modPrefab = modPrefab;
		this.modSprite = modSprite;
		this.price = price;
		this.locked = locked;
		this.modType = modType;
	}

	public GameObject ModPrefab {
		get{ return modPrefab; }
		set{ modPrefab = value; }
	}

	public Sprite ModSprite {
		get{ return modSprite; }
		set{ modSprite = value; }
	}

	public float Price {
		get{ return price; }
		set{ price = value; }
	}

	public bool Locked {
		get{ return locked; }
		set{ locked = value; }
	}

	public Type ModType {
		get{ return modType; }
		set{ modType = value; }
	}
}
