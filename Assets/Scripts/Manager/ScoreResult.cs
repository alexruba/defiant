﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreResult : MonoBehaviour {

	public Text coinsText;
	public Text diamondsText;

	void OnEnable()
	{
		if(GameInformation.IngameCoins == 0)
			coinsText.text = "None";
		else
			coinsText.text = GameInformation.IngameCoins.ToString();

		if(GameInformation.IngameDiamonds == 0)
			diamondsText.text = "None";
		else
			diamondsText.text = GameInformation.IngameDiamonds.ToString();
	}
}
