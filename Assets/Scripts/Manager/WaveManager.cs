﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour {

	public Wave[] waves;
	public float maximumTimeBetweenWave;
	public float minimumTimeBetweenWave;
	public float startWaveTime;
	public static int wavesCount;
	public static int wavesDefeated;

	private float cameraHeight;
	private float cameraWidth;

	void Start()
	{
		cameraHeight = 2f * Camera.main.orthographicSize;
		cameraWidth = cameraHeight * Camera.main.aspect;
		wavesCount = waves.Length;
		
		StartCoroutine ("WaveSpawn");
	}

	void Update()
	{
		if(minimumTimeBetweenWave >= 1f)
			minimumTimeBetweenWave -= 0.005f * Time.deltaTime;
		if(maximumTimeBetweenWave >= 1.2f)
			maximumTimeBetweenWave -= 0.005f * Time.deltaTime;
	}

	IEnumerator WaveSpawn()
	{
		yield return new WaitForSeconds (startWaveTime);

		while(true)
		{
			Instantiate(GetWave(), new Vector2(Random.Range(-cameraWidth/2 + 1.5f, cameraWidth/2 - 1.5f), cameraHeight / 2), Quaternion.identity);
			yield return new WaitForSeconds(Random.Range(minimumTimeBetweenWave, maximumTimeBetweenWave));
		}
	}
	
	public GameObject GetWave()
	{
		float total = 0;
		
		foreach (Wave elem in waves) {
			total += elem.prob;
		}
		
		float randomPoint = Random.value * total;
		
		foreach (Wave elem in waves) {
			if (randomPoint < elem.prob) {
				return elem.gameObject;
			}
			else {
				randomPoint -= elem.prob;
			}
		}

		return waves[waves.Length - 1].gameObject;
	}
}
