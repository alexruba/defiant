﻿using UnityEngine;
using System.Collections;

public class Ally : Spaceship {

	public GameObject bullet;
	private float currentHealth;

	void Start()
	{
		currentHealth = health;
		StartCoroutine ("Fire");
	}

	IEnumerator Fire()
	{
		while (true) {
			GameObject obj = ObjectPoolScript.current.GetObject (bullet);
			if (obj == null) yield break;
			
			obj.transform.position = transform.position;
			obj.transform.rotation = transform.rotation;
			
			ActivateObjectAndChildren(obj);
			
			yield return new WaitForSeconds (shootingRate);
		}
	}

	public void TakeDamage(float damage, float penetration)
	{
		currentHealth -= damage;

		if (currentHealth <= 0) {
			GameObject obj = ObjectPoolScript.current.GetObject (explosion);
			if (obj != null) {
				obj.transform.position = transform.position;
				obj.transform.rotation = transform.rotation;
			}
			obj.SetActive (true);

			Destroy (gameObject, 0.5f);
		}
	}
}
