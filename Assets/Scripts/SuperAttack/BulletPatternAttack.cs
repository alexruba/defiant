using UnityEngine;
using System.Collections;

public class BulletPatternAttack : SuperAttack {

	public GameObject bullet;
	private GameObject effect;

	void Start()
	{
		effect = GameObject.Find ("SuperAttackEffect");
		effect.SetActive (false);
	}
	
	public override void ExecuteSuperAttack()
	{
		StartCoroutine("ChangeStats");
	}
	
	IEnumerator ChangeStats()
	{
		GameObject.Find ("SuperAudio").GetComponent<AudioSource> ().Play ();
		effect.SetActive (true);
		GameObject prevBullet = gameObject.GetComponent<Player> ().bullet;
		gameObject.GetComponent<Player>().bullet = bullet;
		yield return new WaitForSeconds(timeAlive);
		gameObject.GetComponent<Player>().bullet = prevBullet;
		effect.SetActive (false);
		GameObject.Find ("SuperAudio").GetComponent<AudioSource> ().Stop ();
	}
}
