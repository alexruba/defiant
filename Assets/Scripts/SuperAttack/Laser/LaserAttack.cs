﻿using UnityEngine;
using System.Collections;

public class LaserAttack : SuperAttack {
	
	private Player player;
	public GameObject laser;
	public GameObject laserEffect;
	
	void Start()
	{
		GameObject.Find ("SuperAttackEffect").SetActive(false);
		player = gameObject.GetComponent<Player>() as Player;
	}
	
	public override void ExecuteSuperAttack()
	{
		StartCoroutine("Laser");
	}
	
	IEnumerator Laser()
	{
		player.StopCoroutine ("Fire");
		laser.SetActive (true);
		laserEffect.SetActive (true);
		yield return new WaitForSeconds(timeAlive);
		laserEffect.SetActive (false);
		laser.SetActive (false);
		player.StartCoroutine ("Fire");
	}
}
