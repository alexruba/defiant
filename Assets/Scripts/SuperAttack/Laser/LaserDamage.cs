﻿using UnityEngine;
using System.Collections;

public class LaserDamage : MonoBehaviour {

	public float damage;
	public float penetration;
	public GameObject explosion;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals ("Enemy")) {
			Enemy enemy = other.gameObject.GetComponent<Enemy>();
			enemy.TakeDamage(damage, penetration);
			
			if(enemy.health <= 0)
				Player.superAttackCounter += enemy.superAttackBoost;
			
		}
		else if(other.gameObject.tag.Equals("Meteor"))
		{
			GameObject obj = ObjectPoolScript.current.GetObject (explosion);
			if (obj == null) return;
			
			obj.transform.position = other.gameObject.transform.position;
			obj.transform.rotation = other.gameObject.transform.rotation;
			
			obj.SetActive (true);
			
			other.gameObject.SetActive(false);
		}
		
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.gameObject.tag.Equals ("Enemy")) {
			Enemy enemy = other.gameObject.GetComponent<Enemy>();
			enemy.TakeDamage(damage, penetration);
			
			if(enemy.health <= 0)
				Player.superAttackCounter += enemy.superAttackBoost;

		}
		else if(other.gameObject.tag.Equals("Meteor"))
		{
			GameObject obj = ObjectPoolScript.current.GetObject (explosion);
			if (obj == null) return;
			
			obj.transform.position = other.gameObject.transform.position;
			obj.transform.rotation = other.gameObject.transform.rotation;
			
			obj.SetActive (true);

			other.gameObject.SetActive(false);
		}

	}
}
