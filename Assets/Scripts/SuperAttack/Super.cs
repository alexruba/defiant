﻿using UnityEngine;
using System.Collections;

public class Super {

	private SuperAttack superAttack;
	public enum Type{ BulletPattern, Stats, Laser, Ally };
	private Type superType;
	private bool locked;

	public Super(Type superType, SuperAttack superAttack, bool locked)
	{
		this.superType = superType;
		this.superAttack = superAttack;
		this.locked = locked;
	}
	
	public SuperAttack SuperAttack {
		get{ return superAttack;}
		set{ superAttack = value;}
	}
	public Type SuperType {
		get{ return superType;}
		set{ superType = value;}
	}
	public bool Locked {
		get{ return locked;}
		set{ locked = value;}
	}
}
