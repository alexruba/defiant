﻿using UnityEngine;
using System.Collections;

public class AllyAttack : SuperAttack {

	public GameObject ally;
	private float cameraHeight;
	private float cameraWidth;
	
	void Start()
	{
		GameObject.Find ("SuperAttackEffect").SetActive(false);
		cameraHeight = 2f * Camera.main.orthographicSize;
		cameraWidth = cameraHeight * Camera.main.aspect;
	}
	
	public override void ExecuteSuperAttack()
	{
		StartCoroutine("InvokeAllies");
	}
	
	IEnumerator InvokeAllies()
	{
		GameObject.Find ("SuperAudio").GetComponent<AudioSource> ().Play ();
		GameObject ally1 = Instantiate (ally, new Vector3 (-cameraWidth / 2 + 1, -cameraHeight / 2 + 1, 20), Quaternion.identity) as GameObject;
		GameObject ally2 = Instantiate (ally, new Vector3 (cameraWidth / 2 + -1, -cameraHeight / 2 + 1, 20), Quaternion.identity) as GameObject;
		yield return new WaitForSeconds(timeAlive);
		ally1.GetComponent<Ally> ().TakeDamage (1000, 0);
		ally2.GetComponent<Ally> ().TakeDamage (1000, 0);
		GameObject.Find ("SuperAudio").GetComponent<AudioSource> ().Stop ();
	}
}
