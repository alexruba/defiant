﻿using UnityEngine;
using System.Collections;

public class StatsAttack : SuperAttack {

	public float damage = 10;
	public float shootingRate = 0.25f;

	private Player player;

	void Start()
	{
		player = gameObject.GetComponent<Player>() as Player;
	}

	public override void ExecuteSuperAttack()
	{
		StartCoroutine("ChangeStats");
	}

	IEnumerator ChangeStats()
	{
		float damageAux = player.damage;
		float shootingRateAux = player.shootingRate;
		player.damage = damage;
		player.shootingRate = shootingRate;
		yield return new WaitForSeconds(timeAlive);
		player.damage = damageAux;
		player.shootingRate = shootingRateAux;
	}
}
