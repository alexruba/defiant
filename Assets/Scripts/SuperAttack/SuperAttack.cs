﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

abstract public class SuperAttack : MonoBehaviour {

	public float timeAlive;
	
	public void Attack()
	{
		ExecuteSuperAttack();
	}

	public virtual void ExecuteSuperAttack(){}
}
